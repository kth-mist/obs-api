# MIST On-Board Storage API

Contains libraries for accessing data structures in storage devices such as the FRAM, SD cards,
and Norflash.

NOTE: Test cases requires libbsd, gcc, and the ISIS SDK to run. Please update the ISIS-HAL-LINK
variable at the top of the Makefile to your own ISIS-HAL repository. If you do not have such a
repository, you should add the ISIS HAL to the test directory manually before running `make test`.
To clarify, it must have the folder name isis-sdk and if it contains the folders adcs, hal, mission-suppport,
etc., then you can be certain that you have added it correctly.
