/**
 * testdefs.h
 */

#include <stdio.h>
#include <stdlib.h>

#define DBGSTR(s) "%s:%d %s: " s "\n" , __FILE__, __LINE__, __func__
#define ERRSTR(s) "%s:%d %s: \033[1;31m" s "\033[m\n" , __FILE__, __LINE__, __func__
#define OKSTR(s) "%s:%d %s: \033[0;32m" s "\033[m\n" , __FILE__, __LINE__, __func__

#define asserteq(lhs, rhs)                                                      \
	do {                                                                        \
		typeof(lhs) lval = lhs;                                                 \
		typeof(rhs) rval = rhs;                                                 \
		if (lhs != rhs) {                                                       \
			fprintf(stderr, ERRSTR("ASSERTION (%s == %s) FAILED"), #lhs, #rhs); \
			fprintf(stderr, ERRSTR("<lhs> %s was %d"), #lhs, lval);             \
			fprintf(stderr, ERRSTR("<rhs> %s was %d"), #rhs, rval);             \
			exit(1);                                                            \
		}                                                                       \
	} while (0)

#define assertneq(lhs, rhs)                                                     \
	do {                                                                        \
		typeof(lhs) lval = lhs;                                                 \
		typeof(rhs) rval = rhs;                                                 \
		if (lhs == rhs) {                                                       \
			fprintf(stderr, ERRSTR("ASSERTION (%s != %s) FAILED"), #lhs, #rhs); \
			fprintf(stderr, ERRSTR("<lhs> %s was %d"), #lhs, lval);             \
			fprintf(stderr, ERRSTR("<rhs> %s was %d"), #rhs, rval);             \
			exit(1);                                                            \
		}                                                                       \
	} while (0)

#define assertlt(lhs, rhs)                                                      \
	do {                                                                        \
		typeof(lhs) lval = lhs;                                                 \
		typeof(rhs) rval = rhs;                                                 \
		if (!(lhs < rhs)) {                                                     \
			fprintf(stderr, ERRSTR("ASSERTION (%s < %s) FAILED"), #lhs, #rhs);  \
			fprintf(stderr, ERRSTR("<lhs> %s was %d"), #lhs, lval);             \
			fprintf(stderr, ERRSTR("<rhs> %s was %d"), #rhs, rval);             \
			exit(1);                                                            \
		}                                                                       \
	} while (0)


