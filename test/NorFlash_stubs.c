/**
 * NorFlash_stubs.c
 *
 * @brief Contains additional NorFlash stubs not in the HAL clone. This is
 *        used to remove the HAL clone's dependency on the AT91 library
 *        which is compiled for ARMv9. These are minimal and assume the same NorFlash
 *        device as is kept on the flatsat.
 */

#include <at91/boards/ISIS_OBC_G20/board.h>
#include <at91/memories/norflash/NorFlashCFI.h>
#include <at91/memories/norflash/NorFlashApi.h>

void BOARD_ConfigureNorFlash(unsigned char busWidth)
{
	(void) busWidth;
}

unsigned long NorFlash_GetDeviceSizeInBytes(struct NorFlashInfo *pNorFlashInfo)
{
	(void) pNorFlashInfo;

	return BOARD_NORFLASH_SIZE;
}

unsigned int NorFlash_GetDeviceNumOfBlocks(struct NorFlashInfo *pNorFlashInfo)
{
	(void) pNorFlashInfo;

	return 8 + 15;
}

unsigned int NorFlash_GetDeviceBlockSize(struct NorFlashInfo *pNorFlashInfo, unsigned int sector)
{
	(void) pNorFlashInfo;
	(void) sector;

	return sector < 8 ? 0x2000 : 0x10000;
}

unsigned int NorFlash_GetDeviceSectorAddress(struct NorFlashInfo *pNorFlashInfo, unsigned int sector)
{
	(void) pNorFlashInfo;

	return sector < 8 ? sector * 0x2000 : (sector - 7) * 0x10000;
}

void NORFLASH_Reset(struct NorFlash *norFlash, unsigned int address)
{
	norFlash->pOperations->_fReset(&norFlash->norFlashInfo, address);
}

unsigned int NORFLASH_ReadManufactoryID(struct NorFlash *norFlash)
{
	return norFlash->pOperations->_fReadManufactoryID(&norFlash->norFlashInfo);
}

unsigned int NORFLASH_ReadDeviceID(struct NorFlash *norFlash)
{
	return norFlash->pOperations->_fReadDeviceID(&norFlash->norFlashInfo);
}

unsigned char NORFLASH_EraseSector(struct NorFlash *norFlash, unsigned int sectorAddr)
{
	return norFlash->pOperations->_fEraseSector(&norFlash->norFlashInfo, sectorAddr);
}

unsigned char NORFLASH_EraseChip(struct NorFlash *norFlash)
{
	return norFlash->pOperations->_fEraseChip(&norFlash->norFlashInfo);
}

unsigned char NORFLASH_WriteData(struct NorFlash *norFlash, unsigned int address, unsigned char *buffer, unsigned int size)
{
	return norFlash->pOperations->_fWriteData(&norFlash->norFlashInfo, address, buffer, size);
}