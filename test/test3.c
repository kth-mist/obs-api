/**
 * test3.c (norflash)
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <at91/boards/ISIS_OBC_G20/board.h>

#include <obs-api/mist_norflash.h>
#include <obs-api/mist_storage_errors.h>

#include "testdefs.h"

int main()
{
	int ret;
	struct mist_norflash flash;
	uint8_t buf[256];
	uint8_t verify_buf[0x11000];
	size_t i;

	for (i = 0; i < sizeof(buf); i++) {
		buf[i] = i;
	}

	ret = mist_norflash_start(&flash);
	asserteq(ret, 0);
	asserteq(flash.last_erased_sector, 0xFFFF);
	asserteq(flash.last_written_offset, 0xFFFFFFFF);


	ret = mist_norflash_read(&flash, BOARD_NORFLASH_SIZE, buf, 1);
	asserteq(ret, MIST_STORAGE_ERR_PARAM);
	ret = mist_norflash_read(&flash, 0, buf, 0);
	asserteq(ret, MIST_STORAGE_ERR_PARAM);
	ret = mist_norflash_read(NULL, 0, buf, 1);
	asserteq(ret, MIST_STORAGE_ERR_PARAM);
	ret = mist_norflash_read(&flash, 0, NULL, 1);
	asserteq(ret, MIST_STORAGE_ERR_PARAM);
	ret = mist_norflash_write(&flash, BOARD_NORFLASH_SIZE, buf, 1);
	asserteq(ret, MIST_STORAGE_ERR_PARAM);
	ret = mist_norflash_write(&flash, 0, buf, 0);
	asserteq(ret, MIST_STORAGE_ERR_PARAM);
	ret = mist_norflash_write(NULL, 0, buf, 1);
	asserteq(ret, MIST_STORAGE_ERR_PARAM);
	ret = mist_norflash_write(&flash, 0, NULL, 1);
	asserteq(ret, MIST_STORAGE_ERR_PARAM);

	// Write to start of sector, sector should be erased
	ret = mist_norflash_write_and_verify(&flash, 0x10000, buf, sizeof(buf));
	asserteq(ret, 0);
	asserteq(flash.last_written_offset, 0x10000 + sizeof(buf));
	ret = mist_norflash_read(&flash, 0xFFFE, verify_buf, sizeof(verify_buf));
	asserteq(ret, 0);
	for (i = 0; i < sizeof(verify_buf); i++) {
		if (i < 2) {
			// First bytes is in previous sector, so it should not have been erased
			asserteq(verify_buf[i], 0);
		} else if (i < 2 + sizeof(buf)) {
			asserteq(verify_buf[i], buf[i - 2]);
		} else if (i < 0x10002) {
			asserteq(verify_buf[i], 0xFF);
		} else {
			// The rest is in next sector, so it should not have been erased
			asserteq(verify_buf[i], 0);
		}
	}

	// Write to offset and then write to following offset, first write should not be erased
	ret = mist_norflash_write_and_verify(&flash, 0x31110, buf, sizeof(buf));
	asserteq(ret, 0);
	asserteq(flash.last_written_offset, 0x31110 + sizeof(buf));
	ret = mist_norflash_read(&flash, 0x30000, verify_buf, sizeof(verify_buf));
	asserteq(ret, 0);
	for (i = 0; i < sizeof(verify_buf); i++) {
		if (i < 0x1110) {
			asserteq(verify_buf[i], 0xFF);
		} else if (i < 0x1110 + sizeof(buf)) {
			asserteq(verify_buf[i], buf[i - 0x1110]);
		} else if (i < 0x10000) {
			asserteq(verify_buf[i], 0xFF);
		} else {
			// The rest is in next sector, so it should not have been erased
			asserteq(verify_buf[i], 0);
		}
	}
	ret = mist_norflash_write_and_verify(&flash, 0x31110 + sizeof(buf), buf, sizeof(buf));
	asserteq(ret, 0);
	asserteq(flash.last_written_offset, 0x31110 + 2 * sizeof(buf));
	ret = mist_norflash_read(&flash, 0x30000, verify_buf, sizeof(verify_buf));
	asserteq(ret, 0);
	for (i = 0; i < sizeof(verify_buf); i++) {
		if (i < 0x1110) {
			asserteq(verify_buf[i], 0xFF);
		} else if (i < 0x1110 + sizeof(buf)) {
			asserteq(verify_buf[i], buf[i - 0x1110]);
		} else if (i < 0x1110 + sizeof(buf) * 2) {
			asserteq(verify_buf[i], buf[i - 0x1110 - sizeof(buf)]);
		} else if (i < 0x10000) {
			asserteq(verify_buf[i], 0xFF);
		} else {
			// The rest is in next sector, so it should not have been erased
			asserteq(verify_buf[i], 0);
		}
	}

	// Write to offset and then write to not the following offset, first write should be erased
	ret = mist_norflash_write_and_verify(&flash, 0x66000, buf, sizeof(buf));
	asserteq(ret, 0);
	asserteq(flash.last_written_offset, 0x66000 + sizeof(buf));
	ret = mist_norflash_read(&flash, 0x5FFFE, verify_buf, sizeof(verify_buf));
	asserteq(ret, 0);
	for (i = 0; i < sizeof(verify_buf); i++) {
		if (i < 2) {
			asserteq(verify_buf[i], 0);
		} else if (i < 2 + 0x6000) {
			asserteq(verify_buf[i], 0xFF);
		} else if (i < 2 + 0x6000 + sizeof(buf)) {
			asserteq(verify_buf[i], buf[i - 2 - 0x6000]);
		} else if (i < 2 + 0x10000) {
			// The rest is in next sector, so it should not have been erased
			asserteq(verify_buf[i], 0xFF);
		} else {
			asserteq(verify_buf[i], 0);
		}
	}
	ret = mist_norflash_write_and_verify(&flash, 0x66010, buf, sizeof(buf));
	asserteq(ret, 0);
	asserteq(flash.last_written_offset, 0x66010 + sizeof(buf));
	ret = mist_norflash_read(&flash, 0x60000, verify_buf, sizeof(verify_buf));
	asserteq(ret, 0);
	for (i = 0; i < sizeof(verify_buf); i++) {
		if (i < 0x6010) {
			asserteq(verify_buf[i], 0xFF);
		} else if (i < 0x6010 + sizeof(buf)) {
			asserteq(verify_buf[i], buf[i - 0x6010]);
		} else if (i < 0x10000) {
			asserteq(verify_buf[i], 0xFF);
		} else {
			// The rest is in next sector, so it should not have been erased
			asserteq(verify_buf[i], 0);
		}
	}

	// Write between two sectors, both sectors should be erased. Then write to following offset, first write should not be erased.
	ret = mist_norflash_write_and_verify(&flash, 0xEFE80, buf, sizeof(buf));
	asserteq(ret, 0);
	asserteq(flash.last_written_offset, 0xEFE80 + sizeof(buf));
	ret = mist_norflash_read(&flash, 0xE8000, verify_buf, sizeof(verify_buf));
	asserteq(ret, 0);
	for (i = 0; i < sizeof(verify_buf); i++) {
		if (i < 0x7E80) {
			asserteq(verify_buf[i], 0xFF);
		} else if (i < 0x7E80 + sizeof(buf)) {
			asserteq(verify_buf[i], buf[i - 0x7E80]);
		} else if (i < 0x8000) {
			asserteq(verify_buf[i], 0xFF);
		} else {
			// The rest is in next sector, so it should not have been erased
			asserteq(verify_buf[i], 0);
		}
	}
	ret = mist_norflash_write_and_verify(&flash, 0xEFF80, buf, sizeof(buf));
	asserteq(ret, 0);
	asserteq(flash.last_written_offset, 0xEFF80 + sizeof(buf));
	ret = mist_norflash_read(&flash, 0xE8000, verify_buf, sizeof(verify_buf));
	asserteq(ret, 0);
	for (i = 0; i < sizeof(verify_buf); i++) {
		if (i < 0x7E80) {
			asserteq(verify_buf[i], 0xFF);
		} else if (i < 0x7E80 + sizeof(buf)) {
			asserteq(verify_buf[i], buf[i - 0x7E80]);
		} else if (i < 0x7E80 + sizeof(buf) * 2) {
			asserteq(verify_buf[i], buf[i - 0x7E80 - sizeof(buf)]);
		} else {
			asserteq(verify_buf[i], 0xFF);
		}
	}
	ret = mist_norflash_write_and_verify(&flash, 0xF0080, buf, sizeof(buf));
	asserteq(ret, 0);
	asserteq(flash.last_written_offset, 0xF0080 + sizeof(buf));
	ret = mist_norflash_read(&flash, 0xEA000, verify_buf, sizeof(verify_buf));
	asserteq(ret, 0);
	for (i = 0; i < sizeof(verify_buf); i++) {
		if (i < 0x5E80) {
			asserteq(verify_buf[i], 0xFF);
		} else if (i < 0x5E80 + sizeof(buf)) {
			asserteq(verify_buf[i], buf[i - 0x5E80]);
		} else if (i < 0x5E80 + sizeof(buf) * 2) {
			asserteq(verify_buf[i], buf[i - 0x5E80 - sizeof(buf)]);
		} else if (i < 0x5E80 + sizeof(buf) * 3) {
			asserteq(verify_buf[i], buf[i - 0x5E80 - sizeof(buf) * 2]);
		} else {
			asserteq(verify_buf[i], 0xFF);
		}
	}

	// TODO: Test that bug in AT91 library is not present. Write to first byte of sector, then write to the second
	// byte, first byte should not be erased. This requires that it is possible to write just one byte which
	// is currently not possible.

	fprintf(stderr, OKSTR("test passed"));
	return 0;
}
