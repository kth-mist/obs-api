/**
 * test0.c (tmqueue)
 */

#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <obs-api/tmqueue.h>

#include "testdefs.h"

// This exists in tmqueue.c
int tmqueue_metadata_read(int queue_id, struct tmqueue *tmq);

static void addentry(int queue_id, uint16_t len, uint8_t value);
static void pollentry(int queue_id, uint16_t expected_len, uint8_t expected_value);
static void printqueueinfo(int queue_id)
{
	struct tmqueue tmq;
	int ret;

	ret = tmqueue_metadata_read(queue_id, &tmq);
	asserteq(ret, 0);

	fprintf(stderr, "INFO FOR QUEUE %d:\n", queue_id);
	fprintf(stderr, "    entries=%d\n", tmq.entries);
	fprintf(stderr, "    head_blockid=%d\n", tmq.head_blockid);
	fprintf(stderr, "    head_offset=%d\n", tmq.head_offset);
	fprintf(stderr, "    end_blockid=%d\n", tmq.end_blockid);
	fprintf(stderr, "    end_offset=%d\n", tmq.end_offset);
}


int main()
{
	int ret;

	ret = tmqueue_clear(0);
	asserteq(ret, 0);
	ret = tmqueue_clear(1);
	asserteq(ret, 0);
	ret = tmqueue_clear(2);
	asserteq(ret, 0);

	// Insert Queue 0
	for (int i = 0; i < 40; i++) {
		uint16_t addlen = 189; // Size 189 will ensure perfect entry alignment in blocks
		uint8_t addval = (uint8_t) (i + 10);
		addentry(0, addlen, addval);
	}
	asserteq(tmqueue_length(0), 40);

	// Insert Queue 1
	for (int i = 0; i < 400; i++) {
		uint16_t addlen = 43; // Size 43 ensures that metadata is fragmented across 2 blocks for some entries
		uint8_t addval = (uint8_t) (i + 100);
		addentry(1, addlen, addval);
		asserteq(tmqueue_has_error(0), 0);
	}
	asserteq(tmqueue_length(1), 400);

	// Insert Empty
	for (int i = 0; i < 400; i++) {
		uint16_t addlen = 0; // No data
		uint8_t addval = (uint8_t) (i + 100);
		addentry(2, addlen, addval);
		asserteq(tmqueue_has_error(2), 0);
	}
	asserteq(tmqueue_length(2), 400);

	// Poll Queue 0
	for (int i = 0; i < 40; i++) {
		uint16_t polllen = 189;
		uint8_t pollval = (uint8_t) (i + 10);
		pollentry(0, polllen, pollval);
		asserteq(tmqueue_has_error(0), 0);
	}
	asserteq(tmqueue_length(0), 0);

	// Poll Queue 1
	for (int i = 0; i < 400; i++) {
		uint16_t polllen = 43;
		uint8_t pollval = (uint8_t) (i + 100);
		pollentry(1, polllen, pollval);
		asserteq(tmqueue_has_error(0), 0);
	}
	asserteq(tmqueue_length(1), 0);

	// Poll Empty
	for (int i = 0; i < 400; i++) {
		uint16_t polllen = 0;
		uint8_t pollval = (uint8_t) (i + 100);
		pollentry(2, polllen, pollval);
		asserteq(tmqueue_has_error(2), 0);
	}
	asserteq(tmqueue_length(2), 0);

	fprintf(stderr, OKSTR("test passed"));
	return 0;
}

static void addentry(int queue_id, uint16_t len, uint8_t value)
{
	struct tmqueue_entry tme;
	static uint8_t blockbuf[TMQUEUE_BLOCK_SIZE];
	int ret;

	assertlt(len, sizeof(tme.data));

	tme.apid = 0x123;
	tme.service_type = 0x11;
	tme.service_subtype = 0x22;
	tme.timestamp = time(NULL);
	tme.datalen = len;
	memset(tme.data, value, len);

	ret = tmqueue_insert(queue_id, &tme, blockbuf);
	asserteq(ret, 0);
}

static void pollentry(int queue_id, uint16_t expected_len, uint8_t expected_value)
{
	struct tmqueue_entry tme;
	int ret;

	//fprintf(stderr, DBGSTR("expected_value=%d"), expected_value);
	//printqueueinfo(queue_id);
	ret = tmqueue_poll(queue_id, &tme);
	asserteq(ret, 0);

	asserteq(expected_len, tme.datalen);
	for (uint16_t i = 0; i < tme.datalen; i++) {
		asserteq(tme.data[i], expected_value);
	}
}
