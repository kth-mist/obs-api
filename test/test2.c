/**
 * test2.c (persistent tcqueue)
 */

#include <inttypes.h>
#include <stdio.h>
#include <string.h>

#include <obs-api/persistent_tcqueue.h>

#include "testdefs.h"

#define arrlen(arr) (sizeof(arr) / sizeof(arr[0]))

static uint32_t timestamps_insertorder[] = {1500,  342, 4356, 1324,  100, 9753, 9124, 8741};
static uint32_t timestamps_sorted[]      = { 100,  342, 1324, 1500, 4356, 8741, 9124, 9753};

void insert_elements(void);
void poll_elements(void);

int main()
{
	int ret;
	struct persistent_tcqueue_entry e;

	asserteq(PERSISTENT_TCQUEUE_ENTRY_SIZE, sizeof(struct persistent_tcqueue_entry));
	asserteq(PERSISTENT_TCQUEUE_ENTRY_DATA_STRUCT_SIZE, sizeof(struct persistent_tcqueue_entry_data));
	asserteq(PERSISTENT_TCQUEUE_ENTRY_METADATA_SIZE, sizeof(struct persistent_tcqueue_entry_metadata));
	asserteq(PERSISTENT_TCQUEUE_METADATA_SIZE, sizeof(struct persistent_tcqueue));

	ret = persistent_tcqueue_init();
	asserteq(0, ret);

	asserteq(0, persistent_tcqueue_length());
	asserteq(0xFFFFFFFF, persistent_tcqueue_earliest_timestamp());

	asserteq(0, persistent_tcqueue_has_pending(1500));

	insert_elements();
	asserteq(arrlen(timestamps_insertorder), persistent_tcqueue_length());
	asserteq(timestamps_sorted[0], persistent_tcqueue_earliest_timestamp());

	poll_elements();
	asserteq(0, persistent_tcqueue_length());
	asserteq(0xFFFFFFFF, persistent_tcqueue_earliest_timestamp());

	ret = persistent_tcqueue_poll(100000, &e);
	assertneq(0, ret);

	asserteq(0, persistent_tcqueue_length());
	asserteq(0xFFFFFFFF, persistent_tcqueue_earliest_timestamp());

	fprintf(stderr, OKSTR("test passed"));
	return 0;
}


void insert_elements(void)
{
	int ret;
	struct persistent_tcqueue_entry e;

	for (int i = 0; i < arrlen(timestamps_insertorder); i++) {
		e.metadata.timestamp = timestamps_insertorder[i];
		e.metadata.datalen = 30;
		memset(e.data.data, 0x10, 30);

		ret = persistent_tcqueue_insert(&e);
		asserteq(0, ret);
	}
}

void poll_elements(void)
{
	int ret;
	struct persistent_tcqueue_entry e;

	for (int i = 0; i < arrlen(timestamps_sorted); i++) {
		ret = persistent_tcqueue_poll(10000, &e);
		asserteq(0, ret);

		asserteq(e.metadata.timestamp, timestamps_sorted[i]);
		asserteq(e.metadata.datalen, 30);
		for (uint16_t j = 0; j < e.metadata.datalen; j++)
			asserteq(e.data.data[j], 0x10);
	}
}
