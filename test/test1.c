/**
 * test1.c (tcqueue)
 */

#include <inttypes.h>
#include <stdio.h>
#include <string.h>

#include <obs-api/tcqueue.h>

#include "testdefs.h"

#define arrlen(arr) (sizeof(arr) / sizeof(arr[0]))

#define TEST1_CAPACITY (1000)

static uint8_t testqueue[TCQUEUE_MEMORY_SIZE(TEST1_CAPACITY)];

static uint32_t timestamps_insertorder[] = {1500,  342, 4356, 1324,  100, 9753, 9124, 8741};
static uint32_t timestamps_sorted[]      = { 100,  342, 1324, 1500, 4356, 8741, 9124, 9753};

void insert_elements(void);
void poll_elements(void);

int main()
{
	int ret;
	struct tcqueue_entry e;

	asserteq(TCQUEUE_ENTRY_SIZE, sizeof(struct tcqueue_entry));
	asserteq(TCQUEUE_ENTRY_METADATA_SIZE, sizeof(struct tcqueue_entry_metadata));
	asserteq(TCQUEUE_METADATA_SIZE, sizeof(struct tcqueue));

	ret = tcqueue_init(testqueue, TEST1_CAPACITY);
	asserteq(0, ret);

	asserteq(0, tcqueue_length(testqueue));
	asserteq(0xFFFFFFFF, tcqueue_earliest_timestamp(testqueue));

	asserteq(-1, tcqueue_length(NULL));
	asserteq(0xFFFFFFFF, tcqueue_earliest_timestamp(NULL));

	asserteq(0, tcqueue_has_pending(testqueue, 1500));

	insert_elements();
	asserteq(arrlen(timestamps_insertorder), tcqueue_length(testqueue));
	asserteq(timestamps_sorted[0], tcqueue_earliest_timestamp(testqueue));

	poll_elements();
	asserteq(0, tcqueue_length(testqueue));
	asserteq(0xFFFFFFFF, tcqueue_earliest_timestamp(testqueue));

	ret = tcqueue_poll(testqueue, 100000, &e);
	assertneq(0, ret);

	asserteq(0, tcqueue_length(testqueue));
	asserteq(0xFFFFFFFF, tcqueue_earliest_timestamp(testqueue));

	insert_elements();
	asserteq(arrlen(timestamps_insertorder), tcqueue_length(testqueue));
	asserteq(timestamps_sorted[0], tcqueue_earliest_timestamp(testqueue));

	ret = tcqueue_clear(testqueue);
	asserteq(0, ret);
	asserteq(0, tcqueue_length(testqueue));

	fprintf(stderr, OKSTR("test passed"));
	return 0;
}


void insert_elements(void)
{
	int ret;
	struct tcqueue_entry e;

	for (int i = 0; i < arrlen(timestamps_insertorder); i++) {
		e.metadata.timestamp = timestamps_insertorder[i];
		e.metadata.datalen = 30;
		memset(e.data, 0x10, 30);

		ret = tcqueue_insert(testqueue, &e);
		asserteq(0, ret);
	}
}

void poll_elements(void)
{
	int ret;
	struct tcqueue_entry e;

	for (int i = 0; i < arrlen(timestamps_sorted); i++) {
		ret = tcqueue_poll(testqueue, 10000, &e);
		asserteq(0, ret);

		asserteq(e.metadata.timestamp, timestamps_sorted[i]);
		asserteq(e.metadata.datalen, 30);
		for (uint16_t j = 0; j < e.metadata.datalen; j++)
			asserteq(e.data[j], 0x10);
	}
}
