/**
 * @file   persistent_tcqueue.h
 * @author John Wikman
 *
 * A variant of tcqueue.h, but with a fixed capacity, non-volatile storage, and
 * only a single queue. All entries and metadata are stored in FRAM.
 *
 * Besides writing data to FRAM instead of RAM, this implementation also
 * differs in that everything is protected by 8-bit CRC checksums. This means
 * that an entry in the persistent tcqueue will be protected by 2 CRC checksums
 * as both the entry metadata and the entry data will be read separately.
 */

#ifndef PERSISTENT_TCQUEUE_H
#define PERSISTENT_TCQUEUE_H

#include <inttypes.h>
#include <stdlib.h>

#include "mist_storage.h"

/** Size of the persistent tcqueue metadata */
#define PERSISTENT_TCQUEUE_METADATA_SIZE (5)

/** Maximum size of the data for a persistent tcqueue entry (limited by the TRXVU) */
#define PERSISTENT_TCQUEUE_ENTRY_DATA_MAXSIZE (200)

/** Size of the metadata header for a persistent tcqueue entry (timestamp + datalen + 8-bit CRC) */
#define PERSISTENT_TCQUEUE_ENTRY_METADATA_SIZE (7)

/** Size of the persistent tcqueue entry data struct. (+1 for 8-bit CRC) */
#define PERSISTENT_TCQUEUE_ENTRY_DATA_STRUCT_SIZE (PERSISTENT_TCQUEUE_ENTRY_DATA_MAXSIZE + 1)

/** Total size required for a persistent tcqueue entry (+1 for the 8-bit CRC at the end of the data) */
#define PERSISTENT_TCQUEUE_ENTRY_SIZE (PERSISTENT_TCQUEUE_ENTRY_METADATA_SIZE + PERSISTENT_TCQUEUE_ENTRY_DATA_STRUCT_SIZE)

/** Maximum number of entries that can be stored in the persistent tcqueue at
 *  any given time. */
#define PERSISTENT_TCQUEUE_CAPACITY (200)

/** FRAM space required by a tcqueue with the specified entry capacity */
#define PERSISTENT_TCQUEUE_REGION_SIZE (TCQUEUE_METADATA_SIZE + ((max_entries) * TCQUEUE_ENTRY_SIZE))

/** Available FRAM space for the persistent TC queue */
#define PERSISTENT_TCQUEUE_AVAILABLE_FRAM_SPACE (MIST_FRAM_REGION_TC_END_ADDR - MIST_FRAM_REGION_TC_START_ADDR + 1)

/** Sanity check against misconfigurations */
#if (PERSISTENT_TCQUEUE_REGION_SIZE > PERSISTENT_TCQUEUE_AVAILABLE_FRAM_SPACE)
	#error Space required by the persistent tcqueue exceeds the allocated FRAM space.
#endif

/** Error codes returned by the persistent_tcqueue functions. */
#define PERSISTENT_TCQUEUE_ERR_NULL_ARGUMENT           (-1)
#define PERSISTENT_TCQUEUE_ERR_FRAM_WRITE              (-2)
#define PERSISTENT_TCQUEUE_ERR_FRAM_READ               (-3)
#define PERSISTENT_TCQUEUE_ERR_EMPTY                   (-4)
#define PERSISTENT_TCQUEUE_ERR_TOO_EARLY               (-5)
#define PERSISTENT_TCQUEUE_ERR_FULL                    (-6)
#define PERSISTENT_TCQUEUE_ERR_DATA_TOO_BIG            (-7)
#define PERSISTENT_TCQUEUE_ERR_COULD_NOT_TAKE_MUTEX    (-8)
#define PERSISTENT_TCQUEUE_ERR_COULD_NOT_GIVE_MUTEX    (-9)
#define PERSISTENT_TCQUEUE_ERR_COULD_NOT_CREATE_MUTEX (-10)


/** Fatal error codes representing errors that may result in an unrecoverable
 *  state of the queue data structure which must be reinitialized. */
#define PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE  (-102)
#define PERSISTENT_TCQUEUE_FATAL_FRAM_READ   (-103)

/**
 * A struct that contains metadata about the persistent tcqueue.
 */
struct __attribute__((__packed__)) persistent_tcqueue {
	/**
	 * Number of entries in the queue.
	 */
	int32_t length;

	/**
	 * 8-bit CRC as required by mist_storage.h
	 */
	uint8_t crc;
};

/**
 * A struct containing the metadata for a persistent tcqueue entry.
 */
struct __attribute__((__packed__)) persistent_tcqueue_entry_metadata {
	/**
	 * The timestamp of when the telecommand is to be executed.
	 */
	uint32_t timestamp;

	/**
	 * Length of the data stored in this telecommand.
	 */
	uint16_t datalen;

	/**
	 * An 8-bit CRC as required by mist_storage.h. See description at the top
	 * for why the entry metadata has its own CRC.
	 */
	uint8_t crc;
};

/**
 * A struct containing the data for an entry in the persistent tcqueue.
 */
struct __attribute__((__packed__)) persistent_tcqueue_entry_data {
	/**
	 * The data that make up the stored telecommand.
	 */
	uint8_t data[PERSISTENT_TCQUEUE_ENTRY_DATA_MAXSIZE];

	/**
	 * An 8-bit CRC as required by mist_storage.h
	 */
	uint8_t crc;
};

/**
 * A collection struct containing all the information about an entry in the
 * persistent tcqueue.
 */
struct persistent_tcqueue_entry {
	struct persistent_tcqueue_entry_metadata metadata;
	struct persistent_tcqueue_entry_data data;
};

int persistent_tcqueue_init(void);

int32_t persistent_tcqueue_length(void);
uint32_t persistent_tcqueue_earliest_timestamp(void);

int persistent_tcqueue_has_pending(uint32_t current_time);
int persistent_tcqueue_poll(uint32_t current_time, struct persistent_tcqueue_entry *out);

int persistent_tcqueue_insert(struct persistent_tcqueue_entry *in);

#endif /* PERSISTENT_TCQUEUE_H */
