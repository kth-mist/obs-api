/**
 * @file mist_storage.h
 * @author John Wikman
 *
 * Defines storage regions and provides utils that acts as a layer between the
 * HAL and the main part of the code. However, this places the constraint on
 * that all data passed into the provided functions follows a specific data
 * structure format:
 *
 * ---------------------- DATA STRUCTURE FORMAT ----------------------
 *
 * All MIST data structures with CRC must meet the following criteria:
 *  - Must be in the form of a struct with __attribute__((__packed__))
 *  - The CRC field must be exactly one byte in size and be positioned
 *    at the end of the struct.
 *
 * Example:
 * \code{.c}
 * struct __attribute__((__packed__)) mist_data_example {
 *         int foo;
 *         float bar;
 *         char name[20];
 *         unsigned char crc;  // this is the important part!
 * };
 * \endcode
 *
 * Example usage:
 * \code{.c}
 * int write_example(int value, unsigned int address)
 * {
 *         struct mist_data_example ex;
 *         ex.foo = value - 3;
 *         ex.bar = (float) value / 2.0f;
 *         ex.name[0] = 0;
 *
 *         // We do not have to worry about the CRC field. It can be seen as a
 *         // private field that only mist_storage.h uses.
 *
 *         return mist_fram_write_and_generate_crc((unsigned char *) &ex,
 *                                                 address,
 *                                                 sizeof(struct mist_data_example));
 * }
 * \endcode
 *
 * -------------------------------------------------------------------
 */

#ifndef MIST_STORAGE_H
#define MIST_STORAGE_H

#include <inttypes.h>

#include "mist_storage_errors.h"

/* Allocated FRAM region for stored telemetry */
#define MIST_FRAM_REGION_TM_START_ADDR (0x00000)
#define MIST_FRAM_REGION_TM_END_ADDR   (0x0FFFF)

/* Allocated FRAM region for stored non-volatile telecommands */
#define MIST_FRAM_REGION_TC_START_ADDR (0x10000)
#define MIST_FRAM_REGION_TC_END_ADDR   (0x1FFFF)

/* Allocated FRAM region for flight parameters */
#define MIST_FRAM_REGION_PARAM_START_ADDR (0x30000)
#define MIST_FRAM_REGION_PARAM_END_ADDR   (0x3DFFF)

/* Allocated FRAM region for U-boot environment varaibles */
#define MIST_FRAM_REGION_UBOOTENV_START_ADDR (0x3E000)
#define MIST_FRAM_REGION_UBOOTENV_END_ADDR   (0x3EFFF)

/* Allocated FRAM region for redundant flight parameters */
#define MIST_FRAM_REGION_REDUND_PARAM_START_ADDR (0x3F000)
#define MIST_FRAM_REGION_REDUND_PARAM_END_ADDR   (0x3FFFF)

/* How many times to attempt invoking FRAM in case of errors. */
#define MIST_FRAM_ATTEMPTS_READ  (5)
#define MIST_FRAM_ATTEMPTS_WRITE (5)

int mist_generate_crc(unsigned char *src, unsigned int len);
int mist_verify_crc(unsigned char *src, unsigned int len);

int mist_fram_read(unsigned char *dest, unsigned int address, unsigned int size);
int mist_fram_write(unsigned char *src, unsigned int address, unsigned int size);

int mist_fram_read_and_verify_crc(unsigned char *dest, unsigned int address, unsigned int size);
int mist_fram_write_and_generate_crc(unsigned char *src, unsigned int address, unsigned int size);


#define MIST_SDCARD_ATTEMPTS 5
#define MIST_SDCARD_MTU      512
int mist_sdcard_generate_filename(char fname[9], uint32_t id);

int mist_sdcard_dir_exists(const char *dirname);
int mist_sdcard_ensure_dir_exists(const char *dirname);
int mist_sdcard_clear_dir(const char *dirname);
int mist_sdcard_read_id(const char *dirname, uint32_t file_id, uint8_t *dest, uint32_t destsize, uint32_t *bytes_read);
int mist_sdcard_fixedread_id(const char *dirname, uint32_t file_id, uint8_t *dest, uint32_t offset, uint32_t len);
int mist_sdcard_write_id(const char *dirname, uint32_t file_id, const uint8_t *src, uint32_t len);
int mist_sdcard_fixedwrite_id(const char *dirname, uint32_t file_id, const uint8_t *src, uint32_t offset, uint32_t len);
int mist_sdcard_delete_id(const char *dirname, uint32_t file_id);
int mist_sdcard_exists_id(const char *dirname, uint32_t file_id);
int mist_sdcard_create_zeroed_file_id(const char *dirname, uint32_t file_id, uint32_t size);
uint32_t mist_sdcard_length_id(const char *dirname, uint8_t id);


#endif /* MIST_STORAGE_H */
