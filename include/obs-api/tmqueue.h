/**
 * @file   tmqueue.h
 * @author John Wikman
 *
 * A FIFO-style queue that stores telemetry metadata on the SD-card.
 *
 * The structure of entries within the telemetry queue:
 * \code{.unparsed}
 *     |-----------------|
 *  0  |      APID       | 2 bytes
 *  1  |                 |
 *     |-----------------|
 *  2  |  SERVICE  TYPE  | 1 byte
 *     |-----------------|
 *  3  | SERVICE SUBTYPE | 1 byte
 *     |-----------------|
 *  4  |                 |
 *  5  |   UNIX  EPOCH   | 4 bytes
 *  6  |    TIMESTAMP    |
 *  7  |                 |
 *     |-----------------|
 *  8  |      DATA       | 2 bytes
 *  9  |     LENGTH      |
 *     |-----------------|
 * 10  |                 |
 * 11  |      DATA       |
 * 12  |                 | 0 to MAXSIZE bytes
 *     .                 .
 *
 *     .                 .
 *     |-----------------|
 * n-1 |    8-BIT CRC    | 1 byte
 *     |-----------------|
 * \endcode
 *
 * These entries are stored sequentially in 4000 byte blocks. Every single
 * block in the telemetry queue is stored in non-volatile memory, ensuring that
 * no stored telemetry is lost on reset. The last block, where new entries are
 * inserted into the tmqueue, is stored in FRAM. If the last block cannot fit
 * a new entry into the tmqueue, what fits into the last block is inserted
 * there, then the last block is pushed to SD-card, and the remaining data is
 * inserted into a "new" block in FRAM. Hence the blocks acts as a buffer to
 * minimize the number of SD-card writes.
 *
 * To illustrate how the tmqueue, consider the follwing abstract example and
 * state of the tmqueue:
 * \code{.unparsed}
 *   %%%%%%% SD-card %%%%%%%     %% FRAM %%
 *   ----------   ----------     ----------
 *   |      ##|   |########|     |########|
 *   |########|   |########|     |########|
 *   |########|   |########|     |########|
 *   |########|   |########|     |#####   |
 *   ----------   ----------     ----------
 * \endcode
 *   (The empty spaces at the start of the first block indicate that the data
 *    that was stored there has been extracted from the queue. It is never
 *    deleted from the block, but for all functional purposes it can be
 *    considered as deleted. Data in a block is only ever fully deleted from
 *    the SD-card once every entry is extracted from the block.)
 *
 *   When inserting the data ##### the first 3 #'s will be inserted into the
 *   last block, which is then pushed to the SD-card. The last 2 #'s will be
 *   inserted into a new block in FRAM. The new state of the tmqueue:
 *
 * \code{.unparsed}
 *   %%%%%%%%%%%%%% SD-card %%%%%%%%%%%%%     %% FRAM %%
 *   ----------   ----------   ----------     ----------
 *   |      ##|   |########|   |########|     |##      |
 *   |########|   |########|   |########|     |        |
 *   |########|   |########|   |########|     |        |
 *   |########|   |########|   |########|     |        |
 *   ----------   ----------   ----------     ----------
 * \endcode
 *
 *   NOTE: The FRAM space for the new block is the same allocated space as for
 *   the previous block. The size and region occupied by a telemetry queue is
 *   statically allocated and will never change.
 */

#ifndef TMQUEUE_H
#define TMQUEUE_H

#include <inttypes.h>

#include "mist_storage.h"

/** The minimum value of a queue id. */
#define TMQUEUE_MIN_ID (0)

/** The maximum value of a queue id. */
#define TMQUEUE_MAX_ID (7)

/** The maximum number of telemetry queues. */
#define TMQUEUE_MAX_NUMQUEUES (TMQUEUE_MAX_ID - TMQUEUE_MIN_ID + 1)

/** The size of the metadata in an entry.
 *  (APID + ST + SST + TIMESTAMP + SIZE + CRC) */
#define TMQUEUE_ENTRY_METADATA_SIZE (2 + 1 + 1 + 4 + 2 + 1)

/** The size of the metadata that comes before the data in an entry.
 *  (Same as TMQUEUE_ENTRY_METADATA_SIZE but without the CRC) */
#define TMQUEUE_ENTRY_METADATA_HEADERSIZE (TMQUEUE_ENTRY_METADATA_SIZE - 1)

/** Maximum size of the data field in an entry. */
#define TMQUEUE_ENTRY_DATA_MAXSIZE (214)

/** Maximum size of a tmqueue entry */
#define TMQUEUE_ENTRY_MAXSIZE (TMQUEUE_ENTRY_METADATA_SIZE + TMQUEUE_ENTRY_DATA_MAXSIZE)

/** Space allocated in FRAM for a given tmqueue. */
#define TMQUEUE_FRAM_SPACE (4096)

/** The size of a tmqueue block. */
#define TMQUEUE_BLOCK_SIZE (4000)

/** The space allocated in FRAM to house the metadata of a given tmqueue */
#define TMQUEUE_METADATA_REGION_SIZE (TMQUEUE_FRAM_SPACE - TMQUEUE_BLOCK_SIZE)

/** Start address of allocated space in FRAM for the specified queue */
#define TMQUEUE_START_ADDR(queue_id) (MIST_FRAM_REGION_TM_START_ADDR + (queue_id * TMQUEUE_FRAM_SPACE))

/** Start address of the current block in FRAM for the specified queue */
#define TMQUEUE_START_BLOCKADDR(queue_id) (TMQUEUE_START_ADDR(queue_id) + TMQUEUE_METADATA_REGION_SIZE)

/* Compile time assertions */
#if ((TMQUEUE_MAX_NUMQUEUES * TMQUEUE_FRAM_SPACE) \
     >                                            \
     (MIST_FRAM_REGION_TM_END_ADDR - MIST_FRAM_REGION_TM_START_ADDR + 1))
#error Not enough FRAM space to contain the telemetry queues.
#endif
#if (TMQUEUE_METADATA_REGION_SIZE <= TMQUEUE_ENTRY_METADATA_SIZE)
#error Not enough space allocated to hold tmqueue metadata in FRAM.
#endif
#if (TMQUEUE_BLOCK_SIZE < TMQUEUE_ENTRY_MAXSIZE)
#error Block size is smaller than the potential size of an entry.
#endif

#define TMQUEUE_ERR_QUEUE_ID_OOB            (-1)
#define TMQUEUE_ERR_NULL_ARGUMENT           (-2)
#define TMQUEUE_ERR_METADATA_READ           (-3)
#define TMQUEUE_ERR_METADATA_WRITE          (-4)
#define TMQUEUE_ERR_DIR_MISSING             (-5)
#define TMQUEUE_ERR_IS_EMPTY                (-6)
#define TMQUEUE_ERR_ENTRY_FRAM_READ         (-7)
#define TMQUEUE_ERR_ENTRY_FRAM_WRITE        (-8)
#define TMQUEUE_ERR_CRC_MISMATCH            (-9)
#define TMQUEUE_ERR_ENTRY_FS_READ          (-10)
#define TMQUEUE_ERR_ENTRY_FS_WRITE         (-11)
#define TMQUEUE_ERR_READ_INVALID_SIZE      (-12)
#define TMQUEUE_ERR_FS_COULD_NOT_DEL       (-13)
#define TMQUEUE_ERR_DIR_CREATE_FAILED      (-14)
#define TMQUEUE_ERR_DIR_CLEAR_FAILED       (-15)
#define TMQUEUE_ERR_INCONSISTENT_METADATA  (-16)
#define TMQUEUE_ERR_BLOCK_IS_MISSING       (-17)
#define TMQUEUE_ERR_ENTRY_INVALID_SIZE     (-18)
#define TMQUEUE_ERR_COULD_NOT_GENERATE_CRC (-19)
#define TMQUEUE_ERR_COULD_NOT_TAKE_MUTEX   (-20)
#define TMQUEUE_ERR_COULD_NOT_GIVE_MUTEX   (-21)
#define TMQUEUE_ERR_COULD_NOT_CREATE_MUTEX (-22)


/**
 * A struct containing the metadata for a telemetry queue. The id of the queue
 * is not stored with the metadata as it is instead provided in the function
 * calls.
 */
struct __attribute__((__packed__)) tmqueue {
	/**
	 * Number of entries stored in this telemetry queue.
	 */
	uint32_t entries;

	/**
	 * The block id of the block that contain the head entry of the telemetry
	 * queue. (I.e. the block id of the first block in the queue.)
	 */
	uint32_t head_blockid;

	/**
	 * The offset to the head entry of the telemetry queue within the first
	 * block of the queue.
	 */
	uint16_t head_offset;

	/**
	 * The block id of the block where new entries to the queue should be
	 * inserted. (I.e. the block id of the last block in the queue.)
	 */
	uint32_t end_blockid;

	/**
	 * The offset to the end of the telemetry queue in the last block where new
	 * entries are to be inserted.
	 */
	uint16_t end_offset;

	/** An 8-bit CRC last as required by mist_storage.h */
	uint8_t crc;
};

/**
 * A struct for specifying the fields of a tmqueue entry (except the 8-bit
 * CRC). This is never directly serialized and inserted into the queue. It is
 * only intended to be used to specify the data to be inserted or returning the
 * data that was extracted.
 */
struct tmqueue_entry {
	/**
	 * APID specifying the origin of the telemetry.
	 */
	uint16_t apid;

	/**
	 * Service type specifying which service the telemetry came from.
	 */
	uint8_t service_type;

	/**
	 * Service subtype specifying what kind of telemetry that was collected.
	 */
	uint8_t service_subtype;

	/**
	 * A 32-bit unix epoch timestamp specifying when the telemetry was
	 * collected from the source payload.
	 */
	uint32_t timestamp;

	/**
	 * The length of the telemetry data stored in the data field in this
	 * struct.
	 */
	uint16_t datalen;

	/**
	 * The telemetry data. The length of the telemetry data in this buffer is
	 * specified by datalen.
	 */
	uint8_t data[TMQUEUE_ENTRY_DATA_MAXSIZE];
};

int tmqueue_clear(int queue_id);
int tmqueue_has_error(int queue_id);
uint32_t tmqueue_length(int queue_id);

int tmqueue_insert(int queue_id, const struct tmqueue_entry *in, uint8_t *blockbuf);
int tmqueue_poll(int queue_id, struct tmqueue_entry *out);

#endif /* TMQUEUE_H */
