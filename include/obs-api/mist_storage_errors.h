/**
 * @file   mist_storage_errors.h
 * @author John Wikman
 *
 * Error codes for mist_storage.h functions
 */

#ifndef MIST_STORAGE_ERRORS_H
#define MIST_STORAGE_ERRORS_H

#define MIST_STORAGE_OK        (0)
#define MIST_STORAGE_ERR_PARAM (1)

/* The FRAM error codes match those returned in FRAM.h */
#define MIST_STORAGE_ERR_FRAM_LOCK          (-1)
#define MIST_STORAGE_ERR_FRAM_ADDRESS       (-2)
#define MIST_STORAGE_ERR_FRAM_VERIFY        (-3)
#define MIST_STORAGE_ERR_CRC_MISMATCH       (-4)
#define MIST_STORAGE_ERR_SDCARD_GENERAL     (-5)
#define MIST_STORAGE_ERR_SDCARD_DIR         (-6)
#define MIST_STORAGE_ERR_SDCARD_METADATA    (-7)
#define MIST_STORAGE_ERR_SDCARD_READ        (-8)
#define MIST_STORAGE_ERR_SDCARD_WRITE       (-9)
#define MIST_STORAGE_ERR_SDCARD_DELETE     (-10)
#define MIST_STORAGE_ERR_BUFFER_TOO_SMALL  (-11)
#define MIST_STORAGE_ERR_SDCARD_SEEK       (-12)
#define MIST_STORAGE_ERR_NORFLASH_UNKNOWN  (-13)
#define MIST_STORAGE_ERR_NORFLASH_CFI_UNSUPPORTED  (-14)
#define MIST_STORAGE_ERR_NORFLASH_READ     (-15)
#define MIST_STORAGE_ERR_NORFLASH_WRITE    (-17)
#define MIST_STORAGE_ERR_NORFLASH_ERASE    (-18)
#define MIST_STORAGE_ERR_NORFLASH_VERIFY   (-19)
#define MIST_STORAGE_ERR_NORFLASH_ALIGN    (-20)

#endif /* MIST_STORAGE_ERRORS_H */
