/**
 * @file   tcqueue.h
 * @author John Wikman
 *
 * A prioritity queue (smallest first) designed to store telecommands for later
 * execution. The telecommands stored in this queue will be stored in RAM
 * memory and does therefore not persist between reboots of the OBC. There is a
 * separate smaller telecommand queue implementation in persistent_tcqueue.h
 * that stores all of its data in FRAM, meaning that those telecommands are not
 * lost after a reboot of the OBC.
 */

#ifndef TCQUEUE_H
#define TCQUEUE_H

#include <inttypes.h>
#include <stdlib.h>

/** Size of the tcqueue metadata */
#define TCQUEUE_METADATA_SIZE (8)

/** Maximum size of the data for a tcqueue entry (limited by the TRXVU) */
#define TCQUEUE_ENTRY_DATA_MAXSIZE (200)

/** Size of the metadata for a tcqueue entry */
#define TCQUEUE_ENTRY_METADATA_SIZE (6)

/** Total size required for a tcqueue entry */
#define TCQUEUE_ENTRY_SIZE (TCQUEUE_ENTRY_DATA_MAXSIZE + TCQUEUE_ENTRY_METADATA_SIZE)

/** RAM space required by a tcqueue with the specified entry capacity */
#define TCQUEUE_MEMORY_SIZE(max_entries) (TCQUEUE_METADATA_SIZE + ((max_entries) * TCQUEUE_ENTRY_SIZE))

/* Error codes for tcqueue functions */
#define TCQUEUE_ERR_NULL_ARGUMENT    (-1)
#define TCQUEUE_ERR_EMPTY            (-2)
#define TCQUEUE_ERR_TOO_EARLY        (-3)
#define TCQUEUE_ERR_FULL             (-4)
#define TCQUEUE_ERR_DATA_TOO_BIG     (-5)
#define TCQUEUE_ERR_INVALID_CAPACITY (-6)

/**
 * A struct that contains metadata about the queue.
 */
struct __attribute__((__packed__)) tcqueue {
	/**
	 * Number of entries in the queue.
	 */
	int32_t length;

	/**
	 * The capacity of the queue (in number of entries).
	 */
	int32_t capacity;
};

/**
 * A struct that only contains the metadata associated with a tcqueue entry.
 * This is declared separate to the tcqueue_entry struct to avoid allocating
 * more data on the stack than necessary.
 */
struct __attribute__((__packed__)) tcqueue_entry_metadata {
	/**
	 * The timestamp of when the telecommand is to be executed.
	 */
	uint32_t timestamp;

	/**
	 * Length of the data stored in this telecommand.
	 */
	uint16_t datalen;
};

/**
 * A struct containing the information about an entry in the telecommand queue.
 */
struct __attribute__((__packed__)) tcqueue_entry {
	/**
	 * The metadata associated with the entry.
	 */
	struct tcqueue_entry_metadata metadata;

	/**
	 * The data that make up the stored telecommand.
	 */
	uint8_t data[TCQUEUE_ENTRY_DATA_MAXSIZE];
};

int tcqueue_init(uint8_t *memory, int32_t entry_capacity);

int32_t tcqueue_length(const uint8_t *memory);
uint32_t tcqueue_earliest_timestamp(const uint8_t *memory);

int tcqueue_has_pending(const uint8_t *memory, uint32_t current_time);
int tcqueue_poll(uint8_t *memory, uint32_t current_time, struct tcqueue_entry *out);

int tcqueue_insert(uint8_t *memory, const struct tcqueue_entry *in);

int tcqueue_clear(uint8_t *memory);
#endif /* TCQUEUE_H */
