/**
 * @file   mutex.h
 * @author Calin Capitanu
 *
 * An implementation of mutexes for the persistent telecommand queue and
 * the telemetry queue. The functions are declared by the user of this API.
 */

#include <stdint.h>


#define PERSISTENT_TCQUEUE_MUTEX_TIMEOUT 10
#define TMQUEUE_MUTEX_TIMEOUT 10

/**
 * @brief Callback function that creates
 * a mutex for the persistent tcqueue.
 *
 * @return 0 on success and -1 on error.
 */
int persistent_tcqueue_mutex_create(void);

/**
 * @brief Callback function that frees
 * the persistent tcqueue mutex, if
 * existing.
 *
 * @return 0 on success and -1 on error.
 */
int persistent_tcqueue_mutex_give(void);

/**
 * @brief Callback function that attempts
 * to lock the persistent tcqueue mutex,
 * if existing.
 *
 * @param timeout The time (in milliseconds) in which
 * this function attempts to take the mutex.
 *
 * @return 0 on success and -1 on error.
 */
int persistent_tcqueue_mutex_take(uint16_t timeout);

/**
 * @brief Callback function that creates
 * a mutex for the tmqueue.
 *
 * @return 0 on success and -1 on error.
 */
int tmqueue_mutex_create(void);

/**
 * @brief Callback function that frees
 * the tmqueue mutex, if existing.
 *
 * @return 0 on success and -1 on error.
 */
int tmqueue_mutex_give(void);

/**
 * @brief Callback function that attempts
 * to lock the tmqueue mutex, if existing.
 *
 * @param timeout The time (in milliseconds) in which
 * this function attempts to take the mutex.
 *
 * @return 0 on success and -1 on error.
 */
int tmqueue_mutex_take(uint16_t timeout);
