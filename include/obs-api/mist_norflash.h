/**
 * @file mist_storage.h
 * @author William Stackenäs
 *
 * Provides a higher level access to the Norflash built on top
 * of the AT91 library
 *
 * THIS API IS NOT THREAD-SAFE!
 */

#ifndef MIST_NORFLASH_H
#define MIST_NORFLASH_H

#include <stdint.h>
#include <at91/memories/norflash/NorFlashCFI.h>

struct mist_norflash {
	/* Low level NorFlash CFI and driver */
	struct NorFlash cfi;

	/* The last sector that was erased, used to skip
       erasing if not needed */
	uint16_t last_erased_sector;

	/* The last offset that was written to, used to skip
       erasing if not needed */
	uint32_t last_written_offset;
};

int mist_norflash_start(struct mist_norflash *flash);
int mist_norflash_read(struct mist_norflash *flash, uint32_t offset, uint8_t *dst, uint32_t len);
int mist_norflash_write(struct mist_norflash *flash,
                        uint32_t offset,
                        const uint8_t *src,
                        uint32_t len);
int mist_norflash_write_and_verify(struct mist_norflash *flash,
                                   uint32_t offset,
                                   const uint8_t *src,
                                   uint32_t len);

#endif /* MIST_NORFLASH_H */
