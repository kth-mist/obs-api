/**
 * @file   tcqueue.c
 * @author John Wikman
 *
 * Essentially a min-heap that allows inserting and polling. The nodes in the
 * heap is flattened out into an array as follows:
 *
 *                T
 *              /   \
 *            L       R
 *           / \     / \
 *          LL LR   RL RR
 *
 * becomes [T, L, R, LL, LR, RL, RR].
 *
 * If a node N has index i, then the left child of N has index 2*i + 1 and the
 * right child of N has index 2*i + 2. By this formula it is also possible to
 * get the parent index of a node.
 */

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <obs-api/tcqueue.h>

/* Indexing Macros/Functions for the tcqueue implementation */
#define TCQUEUE_MEMORY_OFFSET(i) (TCQUEUE_METADATA_SIZE + ((i) * TCQUEUE_ENTRY_SIZE))

static inline int32_t tcqueue_left_child_idx(int32_t i)
{
	return (2 * i) + 1;
}
static inline int32_t tcqueue_right_child_idx(int32_t i)
{
	return (2 * i) + 2;
}
static inline int32_t tcqueue_parent_idx(int32_t i)
{
	if (i & 1) {
		/* i is odd  =>  i is a left child */
		return (i - 1) / 2;
	} else {
		/* i is even  =>  i is a right child */
		return (i - 2) / 2;
	}
}

/**
 * @brief Initializes a tcqueue at the specified memory region.
 *
 * The specified region must have a capacity of at least
 * TMQUEUE_MEMORY_SIZE(entry_capacity) bytes.
 *
 * @param memory The memory region where the tcqueue should be initialized at.
 * @param entry_capacity The maximum number of entries that the tcqueue should
 *                       be able to store.
 *
 * @return 0 on success. Otherwise an error code from tcqueue.h
 */
int tcqueue_init(uint8_t *memory, int32_t entry_capacity)
{
	struct tcqueue tcq;

	if (memory == NULL)
		return TCQUEUE_ERR_NULL_ARGUMENT;
	if (entry_capacity < 1)
		return TCQUEUE_ERR_INVALID_CAPACITY;

	tcq.length = 0;
	tcq.capacity = entry_capacity;

	memcpy(memory, &tcq, sizeof(tcq));

	return 0;
}

/**
 * @brief Returns the number of entries in the tcqueue.
 *
 * @param memory The memory region where the tcqueue is located.
 *
 * @return The number of entries in the tcqueue. If the memory argument is
 *         NULL, then -1 is returned.
 */
int32_t tcqueue_length(const uint8_t *memory)
{
	struct tcqueue tcq;

	if (memory == NULL)
		return -1;

	memcpy(&tcq, memory, sizeof(tcq));

	return tcq.length;
}

/**
 * @brief Returns the timestamp of the first entry in the queue.
 *
 * @param memory The memory region where the tcqueue is located.
 *
 * @return The timestamp of the first entry in the queue. If the queue is empty
 *         or memory is NULL, then 0xFFFFFFFF is returned.
 */
uint32_t tcqueue_earliest_timestamp(const uint8_t *memory)
{
	struct tcqueue tcq;
	struct tcqueue_entry_metadata emeta;

	if (memory == NULL)
		return 0xFFFFFFFF;

	memcpy(&tcq, memory, sizeof(tcq));

	if (tcq.length == 0)
		return 0xFFFFFFFF;

	memcpy(&emeta, &memory[TCQUEUE_MEMORY_OFFSET(0)], sizeof(emeta));

	return emeta.timestamp;
}

/**
 * @brief Checks if there is a telecommand in the queue ready to execute.
 *
 * @param memory The memory region containing the telecommand queue.
 * @param current_time Reference time to compare against entries in the queue.
 *
 * @return 1 if there is a telecommand ready to be polled from the queue.
 *         Otherwise 0 is returned.
 */
int tcqueue_has_pending(const uint8_t *memory, uint32_t current_time)
{
	struct tcqueue tcq;
	struct tcqueue_entry_metadata emeta;

	if (memory == NULL)
		return 0;

	memcpy(&tcq, memory, sizeof(tcq));

	if (tcq.length == 0)
		return 0;

	memcpy(&emeta, &memory[TCQUEUE_MEMORY_OFFSET(0)], sizeof(emeta));

	if (emeta.timestamp <= current_time)
		return 1;
	else
		return 0;
}

/**
 * @brief Polls an entry from the telecommand queue.
 *
 * @param memory The memory region containing the tcqueue.
 * @param current_time Reference (current) time to compare the entries against.
 * @param out Where the polled entry shall be stored.
 *
 * @return 0 on success. Otherwise an error code from tcqueue.h
 */
int tcqueue_poll(uint8_t *memory, uint32_t current_time, struct tcqueue_entry *out)
{
	struct tcqueue tcq;
	struct tcqueue_entry_metadata emeta;
	int32_t bottom_pos;
	int32_t pos;

	if ((memory == NULL) || (out == NULL))
		return TCQUEUE_ERR_NULL_ARGUMENT;

	memcpy(&tcq, memory, sizeof(tcq));

	if (tcq.length == 0)
		return TCQUEUE_ERR_EMPTY;

	memcpy(&emeta, &memory[TCQUEUE_MEMORY_OFFSET(0)], sizeof(emeta));

	if (emeta.timestamp > current_time)
		return TCQUEUE_ERR_TOO_EARLY;

	/* All looks good, poll the entry from the queue. */
	memcpy(out, &memory[TCQUEUE_MEMORY_OFFSET(0)], sizeof(struct tcqueue_entry));

	if (tcq.length == 1) {
		/* This was the last entry in the queue, do not sift! */
		tcq.length = 0;
		memcpy(memory, &tcq, sizeof(tcq));

		return 0;
	}

	/* Take the bottom entry in the queue and sift it down from the top. */
	bottom_pos = tcq.length - 1;
	memcpy(&emeta, &memory[TCQUEUE_MEMORY_OFFSET(bottom_pos)], sizeof(emeta));

	pos = 0;
	tcq.length -= 1;

	while (pos < tcq.length) {
		struct tcqueue_entry_metadata emeta_chk_left;
		struct tcqueue_entry_metadata emeta_chk_right;

		int32_t left_child_pos = tcqueue_left_child_idx(pos);
		int32_t right_child_pos = tcqueue_right_child_idx(pos);

		int32_t candidate_pos = pos;
		uint32_t candidate_timestamp = emeta.timestamp;

		if (left_child_pos < tcq.length) {
			memcpy(&emeta_chk_left, &memory[TCQUEUE_MEMORY_OFFSET(left_child_pos)], sizeof(emeta_chk_left));
			if (emeta_chk_left.timestamp < candidate_timestamp) {
				candidate_pos = left_child_pos;
				candidate_timestamp = emeta_chk_left.timestamp;
			}
		}

		if (right_child_pos < tcq.length) {
			memcpy(&emeta_chk_right, &memory[TCQUEUE_MEMORY_OFFSET(right_child_pos)], sizeof(emeta_chk_right));
			if (emeta_chk_right.timestamp < candidate_timestamp) {
				candidate_pos = right_child_pos;
				candidate_timestamp = emeta_chk_right.timestamp;
			}
		}

		if (candidate_pos == pos)
			break; // we successfully sifted to our insert position

		/* Move the candidate to the current position and continue the sift at
		 * the candidates previous position. */
		memcpy(&memory[TCQUEUE_MEMORY_OFFSET(pos)],
		       &memory[TCQUEUE_MEMORY_OFFSET(candidate_pos)],
		       TCQUEUE_ENTRY_SIZE);

		pos = candidate_pos;
	}

	/* Copy the sifted entry from the bottom position to the inserti position.
	 * We know that pos < bottom_pos due the < tcq.length checks in conjunction
	 * with the tcq.length -= 1. */
	memcpy(&memory[TCQUEUE_MEMORY_OFFSET(pos)],
	       &memory[TCQUEUE_MEMORY_OFFSET(bottom_pos)],
	       TCQUEUE_ENTRY_SIZE);

	/* Finally, update the metadata of the queue. */
	memcpy(memory, &tcq, sizeof(tcq));

	return 0;
}


/**
 * @brief Inserts an entry into the telecommand queue.
 *
 * @param memory The memory region containing the tcqueue.
 * @param in The entry to be inserted.
 *
 * @return 0 on success. Otherwise an error code from tcqueue.h
 */
int tcqueue_insert(uint8_t *memory, const struct tcqueue_entry *in)
{
	struct tcqueue tcq;
	int32_t pos;

	if ((memory == NULL) || (in == NULL))
		return TCQUEUE_ERR_NULL_ARGUMENT;

	if (in->metadata.datalen > TCQUEUE_ENTRY_DATA_MAXSIZE)
		return TCQUEUE_ERR_DATA_TOO_BIG;

	memcpy(&tcq, memory, sizeof(tcq));

	/* TODO:
	 * Investigate if we should instead replace the largest entry in the queue
	 * with the new entry (if the newer entry has a smaller timestamp) instead
	 * of rejecting the insertion. */
	if (tcq.length == tcq.capacity)
		return TCQUEUE_ERR_FULL;

	/* Start at the bottom, and sift the new entry up */
	tcq.length += 1;
	pos = tcq.length - 1;

	while (pos > 0) {
		struct tcqueue_entry_metadata emeta_parent;

		int32_t parent_pos = tcqueue_parent_idx(pos);

		memcpy(&emeta_parent, &memory[TCQUEUE_MEMORY_OFFSET(parent_pos)], sizeof(emeta_parent));

		/* The parent has a smaller timestamp, we have found our insert position */
		if (emeta_parent.timestamp <= in->metadata.timestamp)
			break;

		/* The new entry has a smaller timestamp than its parent, so they must
		 * swap places. */
		memcpy(&memory[TCQUEUE_MEMORY_OFFSET(pos)],
		       &memory[TCQUEUE_MEMORY_OFFSET(parent_pos)],
		       TCQUEUE_ENTRY_SIZE);

		pos = parent_pos;
	}

	/* Insert the entry at the position generated from the sifting */
	memcpy(&memory[TCQUEUE_MEMORY_OFFSET(pos)], in, TCQUEUE_ENTRY_SIZE);

	/* Update tcqueue metadata */
	memcpy(memory, &tcq, sizeof(tcq));

	return 0;
}

/**
 * @brief Clears (reinitializes) the telecommand queue at the memory address given.
 *
 * The specified region must already be a tcqueue region
 *
 * @param memory The memory region containing the tcqueue
 *
 * @return 0 on success. Otherwise an error code from tcqueue.h
 */
int tcqueue_clear(uint8_t *memory){
	struct tcqueue tcq;

	if(memory == NULL)
		return TCQUEUE_ERR_NULL_ARGUMENT;

	memcpy(&tcq, memory, sizeof(tcq));

	return tcqueue_init(memory, tcq.capacity);
}
