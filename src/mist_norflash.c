/**
 * @file   mist_norflash.c
 * @author William Stackenäs
 */

#include <stdlib.h>
#include <string.h>

#include <at91/boards/ISIS_OBC_G20/board.h>
#include <at91/boards/ISIS_OBC_G20/board_memories.h>

#include <at91/memories/norflash/NorFlashCommon.h>
#include <at91/memories/norflash/NorFlashApi.h>
#include <at91/memories/norflash/NorFlashCFI.h>

#include <obs-api/mist_storage_errors.h>
#include <obs-api/mist_norflash.h>

/* TODO: Make it possible to read from an address, store in a buffer, or read a number of bytes
         that are not aligned by BOARD_NORFLASH_BUSWIDTH. This is not suppored by the At91 library.
		 so a workaround would be needed here. */
#define NORFLASH_IS_MISALIGNED(a) ((unsigned long) a & ((BOARD_NORFLASH_BUSWIDTH >> 3) - 1))

/**
 * This is a replacement of the fuction in the AT91 library. That version of this function has a bug
 * where it mistakenly detects the first byte of a given sector as belonging to the previous sector.
 * This means that if one writes to the second byte of a sector, the first one will always be erased.
 * Likewise, if one writes to the first byte of a sector, it might not be detected that it should be erased
 * first, which leads to the write failing.
 */
static unsigned short NorFlash_GetDeviceSectorInRegion_bugfix(struct NorFlashInfo *info, unsigned int offset)
{
	int i, j;
	unsigned int current_offset = 0;
	unsigned short current_sector = 0;
	struct NorFlashCfiDeviceGeometry *geo;

	geo = &info->cfiDescription.norFlashCfiDeviceGeometry;

	for (i = 0; i < geo->numEraseRegion; i++) {
		for (j = 0; j < geo->eraseRegionInfo[i].Y + 1; j++) {
			current_offset += geo->eraseRegionInfo[i].Z << 8;
			if (current_offset > offset)
				return current_sector;

			current_sector++;
		}
	}

	return current_sector;
}

/**
 * Starts the Norflash and retrieves device information
 */
int mist_norflash_start(struct mist_norflash *flash)
{
	int err;

	if (flash == NULL)
		return MIST_STORAGE_ERR_PARAM;

	flash->cfi.norFlashInfo.baseAddress = BOARD_NORFLASH_BASE_ADDRESS;

	BOARD_ConfigureNorFlash(BOARD_NORFLASH_BUSWIDTH);

	err = NorFlash_CFI_Detect(&flash->cfi, BOARD_NORFLASH_BUSWIDTH >> 3);
	if (err != NorCommon_ERROR_NONE) {
		return MIST_STORAGE_ERR_NORFLASH_UNKNOWN;
	} else if (flash->cfi.norFlashInfo.cfiCompatible == 0) {
		return MIST_STORAGE_ERR_NORFLASH_CFI_UNSUPPORTED;
	}

	flash->last_erased_sector = 0xFFFF;
	flash->last_written_offset = 0xFFFFFFFF;

	return MIST_STORAGE_OK;
}

/**
 * Reads from the Norflash at the given offset.
 */
int mist_norflash_read(struct mist_norflash *flash, uint32_t offset, uint8_t *dst, uint32_t len)
{
	int err;
	uint32_t flashsize;

	if (flash == NULL || dst == NULL)
		return MIST_STORAGE_ERR_PARAM;

	flashsize = (uint32_t) NorFlash_GetDeviceSizeInBytes(&flash->cfi.norFlashInfo);
	if (len == 0 || offset > flashsize || offset + len > flashsize)
		return MIST_STORAGE_ERR_PARAM;

	if (NORFLASH_IS_MISALIGNED(offset) ||
	    NORFLASH_IS_MISALIGNED(dst) ||
	    NORFLASH_IS_MISALIGNED(len)) {
		return MIST_STORAGE_ERR_NORFLASH_ALIGN;
	}

	err = NORFLASH_ReadData(&flash->cfi, (unsigned int) offset, (unsigned char*) dst, (unsigned int) len);
	if (err != NorCommon_ERROR_NONE)
		return MIST_STORAGE_ERR_NORFLASH_READ;

	return MIST_STORAGE_OK;
}

/**
 * Writes to the Norflash at the given offset by first erasing the corresponding sector, if
 * necessary and then writing it. It is assumed that the caller calls this function to sequentially
 * write smaller chunks of data in a linear fashion, i.e. the offset argument of the current call
 * should be set to the offset + len arguments of the previous call. This will make this function
 * remember previous writes to avoid erasing them, but note that it will forget about the
 * previous writes if the caller stops writing in this fashion. It is also the callers
 * responsibility to ensure that no important data is stored before the given offset but in
 * the same sector in the first call to this function, since that data will otherwise be erased.
 * WARNING: THIS FUNCTION IS NOT THREAD-SAFE!
 */
int mist_norflash_write(struct mist_norflash *flash,
                        uint32_t offset,
                        const uint8_t *src,
                        uint32_t len)
{
	int err;
	uint32_t flashsize;
	unsigned short from_sector, to_sector;

	if (flash == NULL || src == NULL)
		return MIST_STORAGE_ERR_PARAM;

	flashsize = (uint32_t) NorFlash_GetDeviceSizeInBytes(&flash->cfi.norFlashInfo);
	if (len == 0 || offset > flashsize || offset + len > flashsize)
		return MIST_STORAGE_ERR_PARAM;

	if (NORFLASH_IS_MISALIGNED(offset) ||
	    NORFLASH_IS_MISALIGNED(src) ||
	    NORFLASH_IS_MISALIGNED(len)) {
		return MIST_STORAGE_ERR_NORFLASH_ALIGN;
	}

	if (offset != flash->last_written_offset)
		flash->last_erased_sector = 0xFFFF;

	to_sector = NorFlash_GetDeviceSectorInRegion_bugfix(&flash->cfi.norFlashInfo, (unsigned int) offset + len - 1);
	for (from_sector = NorFlash_GetDeviceSectorInRegion_bugfix(&flash->cfi.norFlashInfo, (unsigned int) offset);
	     from_sector <= to_sector;
	     from_sector++) {
		if (from_sector == flash->last_erased_sector)
			continue;

		err = NORFLASH_EraseSector(&flash->cfi, NorFlash_GetDeviceSectorAddress(&flash->cfi.norFlashInfo, from_sector));
		if (err != NorCommon_ERROR_NONE) {
			return MIST_STORAGE_ERR_NORFLASH_ERASE;
		}
		flash->last_erased_sector = from_sector;
	}

	err = NORFLASH_WriteData(&flash->cfi, (unsigned int) offset, (unsigned char*) src, (unsigned int) len);
	if (err != NorCommon_ERROR_NONE) {
		return MIST_STORAGE_ERR_NORFLASH_WRITE;
	}

	flash->last_written_offset = offset + len;

	return MIST_STORAGE_OK;
}

int mist_norflash_write_and_verify(struct mist_norflash *flash,
                                   uint32_t offset,
                                   const uint8_t *src,
                                   uint32_t len)
{
	int err;
	uint32_t verify_count = 0;
	uint32_t verify_chunk = 0;
	uint8_t verify_buf[256];
	uint32_t flashsize;

	if (flash == NULL || src == NULL)
		return MIST_STORAGE_ERR_PARAM;

	flashsize = (uint32_t) NorFlash_GetDeviceSizeInBytes(&flash->cfi.norFlashInfo);
	if (len == 0 || offset > flashsize || offset + len > flashsize)
		return MIST_STORAGE_ERR_PARAM;

	if (NORFLASH_IS_MISALIGNED(offset) ||
	    NORFLASH_IS_MISALIGNED(src) ||
	    NORFLASH_IS_MISALIGNED(len)) {
		return MIST_STORAGE_ERR_NORFLASH_ALIGN;
	}

	err = mist_norflash_write(flash, offset, src, len);
	if (err != 0)
		return err;

	for (verify_count = 0; verify_count < len; verify_count += verify_chunk) {
		verify_chunk = len - verify_count > sizeof(verify_buf) ?
		               sizeof(verify_buf) : len - verify_count;

		err = mist_norflash_read(flash, offset + verify_count, verify_buf, verify_chunk);
		if (err != 0) {
			return err;
		} else if (memcmp(verify_buf, src + verify_count, verify_chunk) != 0) {
			return MIST_STORAGE_ERR_NORFLASH_VERIFY;
		}
	}

	return MIST_STORAGE_OK;
}
