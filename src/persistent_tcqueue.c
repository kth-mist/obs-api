/**
 * @file   persistent_tcqueue.c
 * @author John Wikman
 */

#include <inttypes.h>
#include <stdlib.h>

#include <obs-api/persistent_tcqueue.h>
#include <obs-api/mutex.h>

/* Addressing Macros for tcqueue entries */
#define PERSISTENT_TCQUEUE_METADATA_ADDR (MIST_FRAM_REGION_TC_START_ADDR)

#define PERSISTENT_TCQUEUE_ENTRY_METADATA_ADDR(i) (MIST_FRAM_REGION_TC_START_ADDR   +     \
                                                   PERSISTENT_TCQUEUE_METADATA_SIZE +     \
                                                   ((i) * PERSISTENT_TCQUEUE_ENTRY_SIZE))

#define PERSISTENT_TCQUEUE_ENTRY_DATA_ADDR(i) (PERSISTENT_TCQUEUE_ENTRY_METADATA_ADDR(i) + \
                                               PERSISTENT_TCQUEUE_ENTRY_METADATA_SIZE)

static inline int32_t persistent_tcqueue_left_child_idx(int32_t i)
{
	return (2 * i) + 1;
}
static inline int32_t persistent_tcqueue_right_child_idx(int32_t i)
{
	return (2 * i) + 2;
}
static inline int32_t persistent_tcqueue_parent_idx(int32_t i)
{
	if (i & 1) {
		/* i is odd  =>  i is a left child */
		return (i - 1) / 2;
	} else {
		/* i is even  =>  i is a right child */
		return (i - 2) / 2;
	}
}

static uint8_t persistent_tcqueue_mutex_initialized = 0;

/*
 * Function to check whether or not the mutex
 * is initialize and to initialize it if not
 */
static int persistent_tcqueue_mutex_ensure_initialized()
{
	int err;

	if (persistent_tcqueue_mutex_initialized) {
		return 0;
	}

	err = persistent_tcqueue_mutex_create();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	persistent_tcqueue_mutex_initialized = 1;

	return 0;
}


static inline int persistent_tcqueue_read_metadata(struct persistent_tcqueue *ptcq)
{
	return mist_fram_read_and_verify_crc((unsigned char *) ptcq,
	                                     PERSISTENT_TCQUEUE_METADATA_ADDR,
	                                     sizeof(struct persistent_tcqueue));
}

static inline int persistent_tcqueue_read_entry_metadata(struct persistent_tcqueue_entry_metadata *emeta, int32_t pos)
{
	return mist_fram_read_and_verify_crc((unsigned char *) emeta,
	                                     PERSISTENT_TCQUEUE_ENTRY_METADATA_ADDR(pos),
	                                     sizeof(struct persistent_tcqueue_entry_metadata));
}

static inline int persistent_tcqueue_read_entry_data(struct persistent_tcqueue_entry_data *edata, int32_t pos)
{
	return mist_fram_read_and_verify_crc((unsigned char *) edata,
	                                     PERSISTENT_TCQUEUE_ENTRY_DATA_ADDR(pos),
	                                     sizeof(struct persistent_tcqueue_entry_data));
}

static inline int persistent_tcqueue_write_metadata(struct persistent_tcqueue *ptcq)
{
	return mist_fram_write_and_generate_crc((unsigned char *) ptcq,
	                                        PERSISTENT_TCQUEUE_METADATA_ADDR,
	                                        sizeof(struct persistent_tcqueue));
}

static inline int persistent_tcqueue_write_entry_metadata(struct persistent_tcqueue_entry_metadata *emeta, int32_t pos)
{
	return mist_fram_write_and_generate_crc((unsigned char *) emeta,
	                                        PERSISTENT_TCQUEUE_ENTRY_METADATA_ADDR(pos),
	                                        sizeof(struct persistent_tcqueue_entry_metadata));
}

static inline int persistent_tcqueue_write_entry_data(struct persistent_tcqueue_entry_data *edata, int32_t pos)
{
	return mist_fram_write_and_generate_crc((unsigned char *) edata,
	                                        PERSISTENT_TCQUEUE_ENTRY_DATA_ADDR(pos),
	                                        sizeof(struct persistent_tcqueue_entry_data));
}

/**
 * @brief This is the critical section of persistent_tcqueue_init.
 *
 * @return 0 on success. Otherwise an error code from persistent_tcqueue.h
 */
int persistent_tcqueue_init_critical(void)
{
	int ret;
	struct persistent_tcqueue ptcq;

	ptcq.length = 0;

	ret = persistent_tcqueue_write_metadata(&ptcq);
	if (ret != 0) {
		return PERSISTENT_TCQUEUE_ERR_FRAM_WRITE;
	}

	return 0;
}

/**
 * @brief Initializes the persistent tcqueue to an empty state.
 *
 * This can also be used as a method of clearing/resetting the persistent
 * tcqueue in case of errors.
 *
 * @note thread safe
 *
 * @return 0 on success. Otherwise an error code from persistent_tcqueue.h
 */
int persistent_tcqueue_init(void)
{
	int err;
	int ret;

	err = persistent_tcqueue_mutex_ensure_initialized();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = persistent_tcqueue_mutex_take(PERSISTENT_TCQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = persistent_tcqueue_init_critical();

	err = persistent_tcqueue_mutex_give();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}


/**
 * @brief This is the critical section of persistent_tcqueue_length.
 *
 * @return The number of entries in the persistent tcqueue. Otherwise a
 *         negative error code from persistent_tcqueue.h is returned.
 */
int32_t persistent_tcqueue_length_critical(void)
{
	int ret;
	struct persistent_tcqueue ptcq;

	ret = persistent_tcqueue_read_metadata(&ptcq);
	if (ret != 0)
		return 0;

	return ptcq.length;
}

/**
 * @brief Returns the number of entries in the persistent tcqueue.
 *
 * @note thread safe
 *
 * @return The number of entries in the persistent tcqueue. Otherwise a
 *         negative error code from persistent_tcqueue.h is returned.
 */
int32_t persistent_tcqueue_length(void)
{
	int err;
	int ret;

	err = persistent_tcqueue_mutex_ensure_initialized();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = persistent_tcqueue_mutex_take(PERSISTENT_TCQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = persistent_tcqueue_length_critical();

	err = persistent_tcqueue_mutex_give();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}

/**
 * @brief This is the critical section of persistent_tcqueue_earliest_timestamp.
 *
 * @return The timestamp of the first entry in the persistent tcqueue. If the
 *         queue is empty or an error occurred, 0xFFFFFFFF is returned instead.
 */
uint32_t persistent_tcqueue_earliest_timestamp_critical(void)
{
	int ret;
	struct persistent_tcqueue ptcq;
	struct persistent_tcqueue_entry_metadata emeta;

	ret = persistent_tcqueue_read_metadata(&ptcq);
	if (ret != 0)
		return 0xFFFFFFFF;
	if (ptcq.length == 0)
		return 0xFFFFFFFF;

	ret = persistent_tcqueue_read_entry_metadata(&emeta, 0);
	if (ret != 0)
		return 0xFFFFFFFF;

	return emeta.timestamp;
}

/**
 * @brief Returns the timestamp of the first entry in the persistent tcqueue.
 *
 * @note thread safe
 *
 * @return The timestamp of the first entry in the persistent tcqueue. If the
 *         queue is empty or an error occurred, 0xFFFFFFFF is returned instead.
 */
uint32_t persistent_tcqueue_earliest_timestamp(void)
{
	int err;
	uint32_t ret;

	err = persistent_tcqueue_mutex_ensure_initialized();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = persistent_tcqueue_mutex_take(PERSISTENT_TCQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = persistent_tcqueue_earliest_timestamp_critical();

	err = persistent_tcqueue_mutex_give();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}


/**
 * @brief This is the critical section of persistent_tcqueue_has_pending.
 *
 * @param current_time The value of current time to be compared against the
 *                     timestamps of stored entries.
 *
 * @return 1 if there exists an entry in the queue that is ready to be
 *         executed. Otherwise 0 is returned.
 */
int persistent_tcqueue_has_pending_critical(uint32_t current_time)
{
	int ret;
	struct persistent_tcqueue ptcq;
	struct persistent_tcqueue_entry_metadata emeta;

	ret = persistent_tcqueue_read_metadata(&ptcq);
	if (ret != 0)
		return 0;
	if (ptcq.length == 0)
		return 0;

	ret = persistent_tcqueue_read_entry_metadata(&emeta, 0);
	if (ret != 0)
		return 0;

	if (emeta.timestamp <= current_time)
		return 1;
	else
		return 0;
}

/**
 * @brief Check whether a telecommand in the queue is ready to be executed.
 *
 * @note thread safe
 *
 * @param current_time The value of current time to be compared against the
 *                     timestamps of stored entries.
 *
 * @return 1 if there exists an entry in the queue that is ready to be
 *         executed. Otherwise 0 is returned.
 
 */
int persistent_tcqueue_has_pending(uint32_t current_time)
{
	int err;
	int ret;

	err = persistent_tcqueue_mutex_ensure_initialized();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = persistent_tcqueue_mutex_take(PERSISTENT_TCQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = persistent_tcqueue_has_pending_critical(current_time);

	err = persistent_tcqueue_mutex_give();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}

/**
 * @brief This is the critical section of persistent_tcqueue_poll.
 *
 * @param current_time Reference (current) time to compare the entries against.
 * @param out Where the polled entry shall be stored. Note, the value of the
 *            entity pointed to may be modified even if an error occurred.
 *
 * @return 0 on success. Otherwise an error code from persistent_tcqueue.h
 */
int persistent_tcqueue_poll_critical(uint32_t current_time, struct persistent_tcqueue_entry *out)
{
	int ret;
	int32_t pos;
	int32_t bottom_pos;
	struct persistent_tcqueue ptcq;
	struct persistent_tcqueue_entry e;

	if (out == NULL)
		return PERSISTENT_TCQUEUE_ERR_NULL_ARGUMENT;

	ret = persistent_tcqueue_read_metadata(&ptcq);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_ERR_FRAM_READ;

	if (ptcq.length == 0)
		return PERSISTENT_TCQUEUE_ERR_EMPTY;

	/* Load the first entry's metadata. */
	ret = persistent_tcqueue_read_entry_metadata(&out->metadata, 0);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_ERR_FRAM_READ;

	if (out->metadata.timestamp > current_time)
		return PERSISTENT_TCQUEUE_ERR_TOO_EARLY;

	/* It is ready to be executed, load the associated data as well. */
	ret = persistent_tcqueue_read_entry_data(&out->data, 0);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_ERR_FRAM_READ;

	if (ptcq.length == 1) {
		/* Only one entry in the queue, no need for sifting! */
		ptcq.length = 0;
		ret = persistent_tcqueue_write_metadata(&ptcq);
		if (ret != 0)
			return PERSISTENT_TCQUEUE_ERR_FRAM_WRITE;

		return 0;
	}

	/* Now take the last entry in the queue and sift it down from the top. */
	bottom_pos = ptcq.length - 1;
	ret = persistent_tcqueue_read_entry_metadata(&e.metadata, bottom_pos);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_ERR_FRAM_READ;

	pos = 0;
	ptcq.length -= 1;

	/* All errors from here on will be reported as fatal as the data structure
	 * may break on FRAM read/write. */
	while (pos < ptcq.length) {
		struct persistent_tcqueue_entry_metadata emeta_chk_left;
		struct persistent_tcqueue_entry_metadata emeta_chk_right;

		int32_t left_child_pos = persistent_tcqueue_left_child_idx(pos);
		int32_t right_child_pos = persistent_tcqueue_right_child_idx(pos);

		int32_t candidate_pos = pos;
		uint32_t candidate_timestamp = e.metadata.timestamp;

		if (left_child_pos < ptcq.length) {
			ret = persistent_tcqueue_read_entry_metadata(&emeta_chk_left, left_child_pos);
			if (ret != 0)
				return PERSISTENT_TCQUEUE_FATAL_FRAM_READ;

			if (emeta_chk_left.timestamp < candidate_timestamp) {
				candidate_pos = left_child_pos;
				candidate_timestamp = emeta_chk_left.timestamp;
			}
		}

		if (right_child_pos < ptcq.length) {
			ret = persistent_tcqueue_read_entry_metadata(&emeta_chk_right, right_child_pos);
			if (ret != 0)
				return PERSISTENT_TCQUEUE_FATAL_FRAM_READ;

			if (emeta_chk_right.timestamp < candidate_timestamp) {
				candidate_pos = right_child_pos;
				candidate_timestamp = emeta_chk_right.timestamp;
			}
		}

		if (candidate_pos == pos)
			break; // current entry is smaller (or eq) than both of its children!

		/* Use the data field in e to temporarily buffer the candidate data. */
		ret = persistent_tcqueue_read_entry_data(&e.data, candidate_pos);
		if (ret != 0)
			return PERSISTENT_TCQUEUE_FATAL_FRAM_READ;

		/* Write candidate metadata and data to current position */
		if (candidate_pos == left_child_pos) {
			ret = persistent_tcqueue_write_entry_metadata(&emeta_chk_left, pos);
			if (ret != 0)
				return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;
		} else { // (candidate_pos == right_child_pos)
			ret = persistent_tcqueue_write_entry_metadata(&emeta_chk_right, pos);
			if (ret != 0)
				return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;
		}

		ret = persistent_tcqueue_write_entry_data(&e.data, pos);
		if (ret != 0)
			return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;

		/* Update position and keep sifting! (invariant: candidate_pos > pos) */
		pos = candidate_pos;
	}

	/* Now we have arrived at our insert position, extract the data that we
	 * have to move to the new position. */
	ret = persistent_tcqueue_read_entry_data(&e.data, bottom_pos);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_FATAL_FRAM_READ;

	ret = persistent_tcqueue_write_entry_metadata(&e.metadata, pos);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;

	ret = persistent_tcqueue_write_entry_data(&e.data, pos);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;

	/* Sifting complete, write metadata. */
	ret = persistent_tcqueue_write_metadata(&ptcq);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;

	return 0;
}

/**
 * @brief Polls an entry from the persistent tcqueue.
 *
 * @note thread safe
 *
 * @param current_time Reference (current) time to compare the entries against.
 * @param out Where the polled entry shall be stored. Note, the value of the
 *            entity pointed to may be modified even if an error occurred.
 *
 * @return 0 on success. Otherwise an error code from persistent_tcqueue.h
 */
int persistent_tcqueue_poll(uint32_t current_time, struct persistent_tcqueue_entry *out)
{
	int err;
	int ret;

	if (out == NULL)
		return PERSISTENT_TCQUEUE_ERR_NULL_ARGUMENT;

	err = persistent_tcqueue_mutex_ensure_initialized();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = persistent_tcqueue_mutex_take(PERSISTENT_TCQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = persistent_tcqueue_poll_critical(current_time, out);

	err = persistent_tcqueue_mutex_give();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}


/**
 * @brief This is the critical section of persistent_tcqueue_insert.
 *
 * @param in The entry to be inserted. Unlike the tcqueue.h implementation,
 *           this is not declared as const. This is due to the FRAM drivers not
 *           accepting const pointers.
 *
 * @return 0 on success. Otherwise an error code from persistent_tcqueue.h.
 */
int persistent_tcqueue_insert_critical(struct persistent_tcqueue_entry *in)
{
	int ret;
	int32_t pos;
	struct persistent_tcqueue ptcq;

	if (in == NULL)
		return PERSISTENT_TCQUEUE_ERR_NULL_ARGUMENT;

	ret = persistent_tcqueue_read_metadata(&ptcq);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_ERR_FRAM_READ;

	if (in->metadata.datalen > PERSISTENT_TCQUEUE_ENTRY_DATA_MAXSIZE)
		return PERSISTENT_TCQUEUE_ERR_DATA_TOO_BIG;

	if (ptcq.length == PERSISTENT_TCQUEUE_CAPACITY)
		return PERSISTENT_TCQUEUE_ERR_FULL;

	/* Sift the new entry up from the bottom */
	ptcq.length += 1;
	pos = ptcq.length - 1;

	/* All errors from here on are considered fatal due to potential structure failure. */
	while (pos > 0) {
		struct persistent_tcqueue_entry parent;

		int32_t parent_pos = persistent_tcqueue_parent_idx(pos);

		ret = persistent_tcqueue_read_entry_metadata(&parent.metadata, parent_pos);
		if (ret != 0)
			return PERSISTENT_TCQUEUE_FATAL_FRAM_READ;

		if (parent.metadata.timestamp <= in->metadata.timestamp)
			break; /* Parent has smaller (or eq) timestamp, we can insert our new entry here */

		/* Now also load the data for the parent entry and move it to the current position */
		ret = persistent_tcqueue_read_entry_data(&parent.data, parent_pos);
		if (ret != 0)
			return PERSISTENT_TCQUEUE_FATAL_FRAM_READ;

		ret = persistent_tcqueue_write_entry_metadata(&parent.metadata, pos);
		if (ret != 0)
			return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;

		ret = persistent_tcqueue_write_entry_data(&parent.data, pos);
		if (ret != 0)
			return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;

		pos = parent_pos;
	}

	/* We are done sifting, insert the new entry at the current position */
	ret = persistent_tcqueue_write_entry_metadata(&in->metadata, pos);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;

	ret = persistent_tcqueue_write_entry_data(&in->data, pos);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;

	/* Update queue metadata. */
	ret = persistent_tcqueue_write_metadata(&ptcq);
	if (ret != 0)
		return PERSISTENT_TCQUEUE_FATAL_FRAM_WRITE;

	return 0;
}

/**
 * @brief Inserts a new entry into the persistent tcqueue.
 *
 * @note thread safe
 *
 * @param in The entry to be inserted. Unlike the tcqueue.h implementation,
 *           this is not declared as const. This is due to the FRAM drivers not
 *           accepting const pointers.
 *
 * @return 0 on success. Otherwise an error code from persistent_tcqueue.h.
 */
int persistent_tcqueue_insert(struct persistent_tcqueue_entry *in)
{
	int err;
	int ret;

	if (in == NULL)
		return PERSISTENT_TCQUEUE_ERR_NULL_ARGUMENT;

	err = persistent_tcqueue_mutex_ensure_initialized();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = persistent_tcqueue_mutex_take(PERSISTENT_TCQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = persistent_tcqueue_insert_critical(in);

	err = persistent_tcqueue_mutex_give();
	if (err != 0) {
		return PERSISTENT_TCQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}

