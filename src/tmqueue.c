/**
 * @file   tmqueue.c
 * @author John Wikman
 *
 * NOTE:
 * The current implementation is quite clumsy in that it assumes that the queue
 * never will have more than ~4 billion entries stored in the SD-card at any
 * given time. While this is pretty much guaranteed to never happen, the code
 * would look nicer if it covered every single "structural" edge case.
 */

#include <stdio.h>
#include <string.h>

#include <obs-api/tmqueue.h>
#include <obs-api/mist_storage.h>
#include <obs-api/mutex.h>

static uint8_t tmqueue_mutex_initialized = 0;

/*
 * Function to check whether or not the mutex
 * is initialize and to initialize it if not
 */
static int tmqueue_mutex_ensure_initialized()
{
	int err;

	if (tmqueue_mutex_initialized) {
		return 0;
	}

	err = tmqueue_mutex_create();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	tmqueue_mutex_initialized = 1;

	return 0;
}

/* Shorthand function for setting the directory name of the queue_id dir */
static inline void tmqueue_dirname(int queue_id, char *dest, int destsize)
{
	snprintf(dest, destsize, "/MIST_TM/%d", queue_id);
}

/* Wrapper for reading metadata from FRAM */
int tmqueue_metadata_read(int queue_id, struct tmqueue *tmq)
{
	return mist_fram_read_and_verify_crc((unsigned char *) tmq,
	                                     TMQUEUE_START_ADDR(queue_id),
	                                     sizeof(struct tmqueue));
}
/* Wrapper for writing metadata to FRAM */
int tmqueue_metadata_write(int queue_id, struct tmqueue *tmq)
{
	return mist_fram_write_and_generate_crc((unsigned char *) tmq,
	                                        TMQUEUE_START_ADDR(queue_id),
	                                        sizeof(struct tmqueue));
}

/**
 * @brief This is the critical section of tmqueue_clear.
 *
 * @return 0 on success. Otherwise an error code from tmqueue.h
 */
int tmqueue_clear_critical(int queue_id)
{
	int ret;
	char dirname[51];

	struct tmqueue tmq;
	tmq.entries = 0;
	tmq.head_blockid = 0;
	tmq.head_offset = 0;
	tmq.end_blockid = 0;
	tmq.end_offset = 0;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID)){
		return TMQUEUE_ERR_QUEUE_ID_OOB;
	}

	ret = mist_sdcard_ensure_dir_exists("/MIST_TM");
	if (ret != 0) {
		return TMQUEUE_ERR_DIR_CREATE_FAILED;
	}

	tmqueue_dirname(queue_id, dirname, sizeof(dirname));
	ret = mist_sdcard_ensure_dir_exists(dirname);
	if (ret != 0) {
		return TMQUEUE_ERR_DIR_CREATE_FAILED;
	}

	ret = mist_sdcard_clear_dir(dirname);
	if (ret != 0) {
		return TMQUEUE_ERR_DIR_CLEAR_FAILED;
	}

	ret = tmqueue_metadata_write(queue_id, &tmq);
	if (ret != 0) {
		return TMQUEUE_ERR_METADATA_WRITE;
	}

	return 0;
}

/**
 * @brief Clears the telemetry queue.
 *
 * @note thread safe
 *
 * @return 0 on success. Otherwise an error code from tmqueue.h
 */
int tmqueue_clear(int queue_id)
{
	int err;
	int ret;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID)){
		return TMQUEUE_ERR_QUEUE_ID_OOB;
	}

	err = tmqueue_mutex_ensure_initialized();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = tmqueue_mutex_take(TMQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = tmqueue_clear_critical(queue_id);

	err = tmqueue_mutex_give();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}

/**
 * @brief This is the critical section of tmqueue_has_error.  
 *
 * @return 0 if the queue has no error. Otherwise an error code from
 *         tmqueue.h
 */
int tmqueue_has_error_critical(int queue_id)
{
	int ret;
	char dirname[51];
	struct tmqueue tmq;
	uint32_t blockid;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID)) {
		return TMQUEUE_ERR_QUEUE_ID_OOB;
	}

	ret = tmqueue_metadata_read(queue_id, &tmq);
	if (ret) {
		return TMQUEUE_ERR_METADATA_READ;
	}

	tmqueue_dirname(queue_id, dirname, sizeof(dirname));
	if (!mist_sdcard_dir_exists(dirname)) {
		return TMQUEUE_ERR_DIR_MISSING;
	}

	if (tmq.head_offset >= TMQUEUE_BLOCK_SIZE) {
		return TMQUEUE_ERR_INCONSISTENT_METADATA; /* offset is outside of block */
	}
	
	if (tmq.end_offset >= TMQUEUE_BLOCK_SIZE) {
		return TMQUEUE_ERR_INCONSISTENT_METADATA; /* current block is full */
	}

	for (blockid = tmq.head_blockid; blockid != tmq.end_blockid; blockid++) {
		if (!mist_sdcard_exists_id(dirname, blockid)) {
			return TMQUEUE_ERR_BLOCK_IS_MISSING;
		}
	}

	return 0; /* OK */
}

/**
 * @brief Checks if the structure of a tmqueue is erroneous.
 *
 * @note thread safe
 *
 * @return 0 if the queue has no error. Otherwise an error code from
 *         tmqueue.h
 */
int tmqueue_has_error(int queue_id)
{
	int err;
	int ret;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID)) {
		return TMQUEUE_ERR_QUEUE_ID_OOB;
	}

	err = tmqueue_mutex_ensure_initialized();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = tmqueue_mutex_take(TMQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = tmqueue_has_error_critical(queue_id);

	err = tmqueue_mutex_give();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}

/**
 * @brief This is the critical section of tmqueue_length.
 *
 * @return the number of entries in the tmqueue. If there was an error reading
 * metadata from FRAM, then a length of 0 is returned. Use the function
 * tmqueue_has_error instead to determine if the queue is erroneous.
 */
uint32_t tmqueue_length_critical(int queue_id)
{
	int ret;
	struct tmqueue tmq;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID))
		return 0;

	ret = tmqueue_metadata_read(queue_id, &tmq);
	if (ret)
		return 0;

	return tmq.entries;
}

/**
 * Return the number of entries in the tmqueue. If there was an error reading
 * metadata from FRAM, then a length of 0 is returned. Use the function
 * tmqueue_has_error instead to determine if the queue is erroneous.
 *
 * @note thread safe
 *
 * @return length of tmqueue or negative error code from tmqueue.h
 */
uint32_t tmqueue_length(int queue_id)
{
	int err;
	uint32_t ret;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID))
		return 0;

	err = tmqueue_mutex_ensure_initialized();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = tmqueue_mutex_take(TMQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = tmqueue_length_critical(queue_id);

	err = tmqueue_mutex_give();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}

/**
 * @brief This is the critical section of tmqueue_insert.
 *
 * @param queue_id The id of the telemetry queue.
 * @param in       A filled out entry to be inserted into the telemetry queue.
 * @param blockbuf A pointer to a buffer capable of storing at least 4000 bytes
 *                 (TMQUEUE_BLOCK_SIZE). This is used for when blocks are moved
 *                 from FRAM to the SD-cards.
 *
 * @return 0 on success. Otherwise an error code from tmqueue.h
 */
int tmqueue_insert_critical(int queue_id, const struct tmqueue_entry *in, uint8_t *blockbuf)
{
	int ret;
	char dirname[51];
	struct tmqueue tmq;
	unsigned char entry[TMQUEUE_ENTRY_MAXSIZE];
	unsigned int size;
	unsigned int address;
	unsigned int rem_block_space;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID)) {
		return TMQUEUE_ERR_QUEUE_ID_OOB;
	}

	if ((in == NULL) || (blockbuf == NULL)) {
		return TMQUEUE_ERR_NULL_ARGUMENT;
	}

	if (in->datalen > TMQUEUE_ENTRY_DATA_MAXSIZE) {
		return TMQUEUE_ERR_ENTRY_INVALID_SIZE;
	}

	tmqueue_dirname(queue_id, dirname, sizeof(dirname));
	if (!mist_sdcard_dir_exists(dirname)) {
		return TMQUEUE_ERR_DIR_MISSING;
	}

	ret = tmqueue_metadata_read(queue_id, &tmq);
	if (ret != 0) {
		return TMQUEUE_ERR_METADATA_READ;
	}

	// Sanity check on tmqueue fields
	if (tmq.end_offset >= TMQUEUE_BLOCK_SIZE) {
		return TMQUEUE_ERR_INCONSISTENT_METADATA;
	}

	size = TMQUEUE_ENTRY_METADATA_SIZE + in->datalen;

	memcpy(&entry[0], &in->apid, sizeof(in->apid));
	entry[2] = (unsigned char) in->service_type;
	entry[3] = (unsigned char) in->service_subtype;
	memcpy(&entry[4], &in->timestamp, sizeof(in->timestamp));
	memcpy(&entry[8], &in->datalen, sizeof(in->datalen));
	memcpy(&entry[10], in->data, in->datalen);

	// Generate the CRC manually (since it is used in both cases)
	ret = mist_generate_crc(entry, size);
	if (ret != 0) {
		return TMQUEUE_ERR_COULD_NOT_GENERATE_CRC;
	}

	// Check if we are in a situation where we need to push data to the SD-card
	// or if there will be space over in the block in FRAM:
	rem_block_space = TMQUEUE_BLOCK_SIZE - tmq.end_offset;
	if (rem_block_space > size) {
		/* There is room left in the current block for the whole entry */

		address = TMQUEUE_START_BLOCKADDR(queue_id) + tmq.end_offset;

		ret = mist_fram_write(entry, address, size);
		if (ret != 0) {
			return TMQUEUE_ERR_METADATA_WRITE;
		}

		// Update metadata fields
		tmq.end_offset += size;
	} else {
		/* The current block will be full after inserting the current entry
		 * INVARIANT: size >= rem_block_space > 0 (due to data structure) */

		// The remaining block space is also the size of the initial data to be
		// put into the first block
		unsigned int initial_size = rem_block_space;

		// Compute the remaining size that has to be put into the second block
		unsigned int remaining_size = size - initial_size;

		// First fetch the entire end/current block from FRAM into RAM
		address = TMQUEUE_START_BLOCKADDR(queue_id);
		ret = mist_fram_read(blockbuf, address, tmq.end_offset);
		if (ret != 0) {
			return TMQUEUE_ERR_METADATA_READ;
		}

		// Copy the initial part into the fetched block and push it to the
		// SD-card
		memcpy(&blockbuf[tmq.end_offset], entry, initial_size);

		ret = mist_sdcard_write_id(dirname, tmq.end_blockid, blockbuf, TMQUEUE_BLOCK_SIZE);
		if (ret != 0) {
			return TMQUEUE_ERR_ENTRY_FS_WRITE;
		}

		if (remaining_size > 0) {
			// There is a remaining part, insert it into FRAM
			address = TMQUEUE_START_BLOCKADDR(queue_id);
			ret = mist_fram_write(&entry[initial_size], address, remaining_size);
			if (ret != 0) {
				return TMQUEUE_ERR_ENTRY_FRAM_WRITE;
			}
		}

		// All looks good, update metadata fields
		tmq.end_blockid += 1;
		tmq.end_offset = remaining_size;
	}

	// Both if statements should be successful at this point, so write new
	// metadata to FRAM
	tmq.entries += 1;
	ret = tmqueue_metadata_write(queue_id, &tmq);
	if (ret != 0) {
		return TMQUEUE_ERR_METADATA_WRITE;
	}

	return 0;
}


/**
 * Inserts an entry into the telemetry queue with the specified id.
 *
 * @note thread safe
 *
 * @param queue_id The id of the telemetry queue.
 * @param in       A filled out entry to be inserted into the telemetry queue.
 * @param blockbuf A pointer to a buffer capable of storing at least 4000 bytes
 *                 (TMQUEUE_BLOCK_SIZE). This is used for when blocks are moved
 *                 from FRAM to the SD-cards.
 *
 * @return 0 on success. Otherwise an error code from tmqueue.h
 */
int tmqueue_insert(int queue_id, const struct tmqueue_entry *in, uint8_t *blockbuf)
{
	int err;
	int ret;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID)) {
		return TMQUEUE_ERR_QUEUE_ID_OOB;
	}

	if ((in == NULL) || (blockbuf == NULL)) {
		return TMQUEUE_ERR_NULL_ARGUMENT;
	}

	if (in->datalen > TMQUEUE_ENTRY_DATA_MAXSIZE) {
		return TMQUEUE_ERR_ENTRY_INVALID_SIZE;
	}

	err = tmqueue_mutex_ensure_initialized();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = tmqueue_mutex_take(TMQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = tmqueue_insert_critical(queue_id, in, blockbuf);

	err = tmqueue_mutex_give();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}

/**
 * @brief This is the critical section of tmqueue_poll.	
 *
 * @param queue_id The id of the telemetry queue.
 * @param out      The entry where the polled data is to be inserted.
 *
 * @return  0 on success. Otherwise a negative error code from tmqueue.h.
 */
int tmqueue_poll_critical(int queue_id, struct tmqueue_entry *out)
{
	int ret;
	int attempts;
	char dirname[51];
	struct tmqueue tmq;
	unsigned char entry[TMQUEUE_ENTRY_MAXSIZE];
	unsigned int size = 0;
	unsigned int address;
	uint16_t datalen;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID)) {
		return TMQUEUE_ERR_QUEUE_ID_OOB;
	}

	if (out == NULL) {
		return TMQUEUE_ERR_NULL_ARGUMENT;
	}

	tmqueue_dirname(queue_id, dirname, sizeof(dirname));
	if (!mist_sdcard_dir_exists(dirname)) {
		return TMQUEUE_ERR_DIR_MISSING;
	}

	ret = tmqueue_metadata_read(queue_id, &tmq);
	if (ret != 0) {
		return TMQUEUE_ERR_METADATA_READ;
	}

	if (tmq.entries == 0) {
		return TMQUEUE_ERR_IS_EMPTY; // queue is empty
	}

	if (tmq.head_blockid == tmq.end_blockid) {
		// The queue only has a single block which is stored in FRAM.
		attempts = 0;
		do {
			attempts++;

			// Read only the metadata first (except the CRC)
			address = TMQUEUE_START_BLOCKADDR(queue_id) + tmq.head_offset;
			ret = mist_fram_read(entry, address, TMQUEUE_ENTRY_METADATA_SIZE - 1);
			if (ret != 0) {
				ret = TMQUEUE_ERR_ENTRY_FRAM_READ;
				continue;
			}

			memcpy(&datalen, &entry[8], sizeof(datalen));
			if (datalen > TMQUEUE_ENTRY_DATA_MAXSIZE) {
				ret = TMQUEUE_ERR_READ_INVALID_SIZE;
				continue; // length is too large, probably a bit error occurred
			}

			size = datalen + TMQUEUE_ENTRY_METADATA_SIZE;

			// Read the rest of the entry and the CRC
			address += TMQUEUE_ENTRY_METADATA_SIZE - 1;
			ret = mist_fram_read(&entry[TMQUEUE_ENTRY_METADATA_SIZE - 1], address, datalen + 1);
			if (ret != 0) {
				ret = TMQUEUE_ERR_ENTRY_FRAM_READ;
				continue;
			}

			if (!mist_verify_crc(entry, size)) {
				ret = TMQUEUE_ERR_CRC_MISMATCH;
				continue; // CRC is invalid
			}
		} while ((ret != 0) && (attempts < MIST_FRAM_ATTEMPTS_READ));

		if (ret != 0) {
			return ret; // Could not retrieve entry
		}
	} else {
		// Need to fetch initial metadata from the SD-card
		int metadata_on_fstblk = 0;
		int metadata_on_sndblk = 0;
		int remaining_on_fstblk = TMQUEUE_BLOCK_SIZE - tmq.head_offset;

		if (remaining_on_fstblk < TMQUEUE_ENTRY_METADATA_HEADERSIZE) {
			metadata_on_fstblk = remaining_on_fstblk;
			metadata_on_sndblk = TMQUEUE_ENTRY_METADATA_HEADERSIZE - metadata_on_fstblk;
		} else {
			metadata_on_fstblk = TMQUEUE_ENTRY_METADATA_HEADERSIZE;
			metadata_on_sndblk = 0;
		}

		attempts = 0;
		do { // while ((ret != 0) && (attempts < MIST_FRAM_ATTEMPTS_READ));
			int data_and_crc_on_fstblk = 0;
			int data_and_crc_on_sndblk = 0;
			int offset_fstblk = 0;
			int offset_sndblk = 0;

			attempts++;

			ret = mist_sdcard_fixedread_id(dirname,
			                               tmq.head_blockid,
			                               entry,
			                               tmq.head_offset,
			                               metadata_on_fstblk);
			if (ret != 0) {
				ret = TMQUEUE_ERR_ENTRY_FS_READ;
				continue;
			}

			// Read remainder of metadata if fragmented across multiple blocks
			if (metadata_on_sndblk > 0) {
				if ((tmq.head_blockid + 1) == tmq.end_blockid) {
					// Next block is in FRAM
					address = TMQUEUE_START_BLOCKADDR(queue_id);
					ret = mist_fram_read(&entry[metadata_on_fstblk], address, metadata_on_sndblk);
					if (ret != 0) {
						ret = TMQUEUE_ERR_ENTRY_FRAM_READ;
						continue;
					}
				} else {
					// Next block is also on the SD-card
					ret = mist_sdcard_fixedread_id(dirname,
					                               tmq.head_blockid + 1,
					                               &entry[metadata_on_fstblk],
					                               0,
					                               metadata_on_sndblk);
					if (ret != 0) {
						ret = TMQUEUE_ERR_ENTRY_FS_READ;
						continue;
					}
				}
			}

			memcpy(&datalen, &entry[8], sizeof(datalen));
			if (datalen > TMQUEUE_ENTRY_DATA_MAXSIZE) {
				ret = TMQUEUE_ERR_READ_INVALID_SIZE;
				continue; // length is too large, a bit error has probably occurred
			}

			size = datalen + TMQUEUE_ENTRY_METADATA_SIZE;

			if (metadata_on_sndblk > 0) {
				// Part of the metadata is on the second block, so the data and
				// CRC must also be there.
				data_and_crc_on_fstblk = 0;
				data_and_crc_on_sndblk = datalen + 1;
				offset_fstblk = 0;
				offset_sndblk = metadata_on_sndblk;
			} else {
				data_and_crc_on_fstblk = remaining_on_fstblk - metadata_on_fstblk;
				if (data_and_crc_on_fstblk >= (int) (datalen + 1)) {
					// Data and CRC is also on first block
					data_and_crc_on_fstblk = datalen + 1;
					data_and_crc_on_sndblk = 0;
					offset_fstblk = tmq.head_offset + metadata_on_fstblk;
					offset_sndblk = 0;
				} else if (data_and_crc_on_fstblk == 0) {
					// Data field starts at the start of the next block
					data_and_crc_on_sndblk = datalen + 1;
					offset_fstblk = 0;
					offset_sndblk = 0;
				} else {
					// Data and CRC is fragmented across both blocks
					data_and_crc_on_sndblk = (datalen + 1) - data_and_crc_on_fstblk;
					offset_fstblk = tmq.head_offset + metadata_on_fstblk;
					offset_sndblk = 0;
				}
			}

			// Read data and CRC now
			if (data_and_crc_on_fstblk > 0) {
				// First block is guaranteed on the SD-card
				ret = mist_sdcard_fixedread_id(dirname,
				                               tmq.head_blockid,
				                               &entry[10],
				                               offset_fstblk,
				                               data_and_crc_on_fstblk);
				if (ret != 0) {
					ret = TMQUEUE_ERR_ENTRY_FS_READ;
					continue;
				}
			}
			if (data_and_crc_on_sndblk > 0) {
				if ((tmq.head_blockid + 1) == tmq.end_blockid) {
					// Next block is in FRAM
					address = TMQUEUE_START_BLOCKADDR(queue_id) + offset_sndblk;
					ret = mist_fram_read(&entry[10 + data_and_crc_on_fstblk],
					                     address,
					                     data_and_crc_on_sndblk);
					if (ret != 0) {
						ret = TMQUEUE_ERR_ENTRY_FRAM_READ;
						continue;
					}
				} else {
					// Next block is on the SD-card
					ret = mist_sdcard_fixedread_id(dirname,
					                               tmq.head_blockid + 1,
					                               &entry[10 + data_and_crc_on_fstblk],
					                               offset_sndblk,
					                               data_and_crc_on_sndblk);
					if (ret != 0) {
						ret = TMQUEUE_ERR_ENTRY_FS_READ;
						continue;
					}
				}
			}

			if (!mist_verify_crc(entry, size)) {
				ret = TMQUEUE_ERR_CRC_MISMATCH;
				continue; // CRC is invalid
			}
		} while ((ret != 0) && (attempts < MIST_FRAM_ATTEMPTS_READ));

		if (ret != 0) {
			return ret; // Could not retrieve fragmented entry
		}
	}

	// INVARIANT: Entry is correctly retrieved from queue, and the "size"
	// variable is set.

	// Update metadata
	tmq.entries -= 1;
	tmq.head_offset += size;
	if (tmq.head_offset >= TMQUEUE_BLOCK_SIZE) {
		// The first block is empty, delete it from the filesystem
		ret = mist_sdcard_delete_id(dirname, tmq.head_blockid);
		if (ret != 0) {
			return TMQUEUE_ERR_FS_COULD_NOT_DEL;
		}

		tmq.head_blockid += 1;
		tmq.head_offset -= TMQUEUE_BLOCK_SIZE;
	}

	ret = tmqueue_metadata_write(queue_id, &tmq);
	if (ret != 0)
		return TMQUEUE_ERR_METADATA_WRITE;

	// Everything went successfully, return the extracted entry.
	memcpy(&out->apid, &entry[0], sizeof(out->apid));
	out->service_type = entry[2];
	out->service_subtype = entry[3];
	memcpy(&out->timestamp, &entry[4], sizeof(out->timestamp));
	memcpy(&out->datalen, &entry[8], sizeof(out->datalen));
	memcpy(out->data, &entry[10], out->datalen);

	return 0;
}

/**
 * Polls an entry from the telemetry queue with the specified id, removing it
 * from the queue and returning the data in the specified out parameter.
 *
 * @note thread safe
 *
 * @param queue_id The id of the telemetry queue.
 * @param out      The entry where the polled data is to be inserted.
 *
 * @return  0 on success. Otherwise a negative error code from tmqueue.h.
 */
int tmqueue_poll(int queue_id, struct tmqueue_entry *out)
{
	int err;
	int ret;

	if ((queue_id < TMQUEUE_MIN_ID) || (queue_id > TMQUEUE_MAX_ID)) {
		return TMQUEUE_ERR_QUEUE_ID_OOB;
	}

	if (out == NULL) {
		return TMQUEUE_ERR_NULL_ARGUMENT;
	}

	err = tmqueue_mutex_ensure_initialized();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_CREATE_MUTEX;
	}

	err = tmqueue_mutex_take(TMQUEUE_MUTEX_TIMEOUT);
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_TAKE_MUTEX;
	}

	ret = tmqueue_poll_critical(queue_id, out);

	err = tmqueue_mutex_give();
	if (err != 0) {
		return TMQUEUE_ERR_COULD_NOT_GIVE_MUTEX;
	}

	return ret;
}

