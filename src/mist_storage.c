/**
 * @file   mist_storage.c
 * @author John Wikman
 */

#include <hal/boolean.h>
#include <hal/checksum.h>
#include <hal/Storage/FRAM.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>
#include <hcc/api_hcc_mem.h>
#include <hcc/api_mdriver_atmel_mcipdc.h>

#include <stdlib.h>

#include <obs-api/mist_storage.h>
#include <obs-api/mist_storage_errors.h>

static int has_lookuptable = 0;
static unsigned char crc8_lookuptable[256];

/* Verifies that the lookuptable is calculated */
static inline void verify_lookuptable(void)
{
	if (!has_lookuptable) {
		checksum_prepareLUTCRC8(CRC8_POLYNOMIAL, crc8_lookuptable);
		has_lookuptable = 1;
	}
}

/* Shorthand function for calculating crc */
static inline unsigned char calculate_crc(unsigned char *data, unsigned int size)
{
	return checksum_calculateCRC8LUT(data, size, crc8_lookuptable, CRC8_DEFAULT_STARTREMAINDER, TRUE);
}

static long f_read_mtu(uint8_t *buf, long size, FN_FILE * file)
{
	long total_read = 0;
	long expected_read = 0;
	long actual_read = 0;

	while (total_read < size) {
		expected_read = size - total_read < MIST_SDCARD_MTU ? size - total_read : MIST_SDCARD_MTU;

		actual_read = f_read(buf + total_read, sizeof(uint8_t), expected_read, file);
		total_read += actual_read;
		if (actual_read != expected_read) {
			return total_read;
		}
	}

	return total_read;
}

static long f_write_mtu(const uint8_t *buf, long size, FN_FILE * file)
{
	long total_written = 0;
	long expected_written = 0;
	long actual_written = 0;

	while (total_written < size) {
		expected_written = size - total_written < MIST_SDCARD_MTU ? size - total_written : MIST_SDCARD_MTU;

		actual_written = f_write(buf + total_written, sizeof(uint8_t), expected_written, file);
		total_written += actual_written;
		if (actual_written != expected_written) {
			return total_written;
		}
	}

	return total_written;
}

/**
 * Generates an 8-bit CRC based on the len - 1 first bytes in src. The resuting
 * CRC is inserted at the end of src.
 *
 * IMPORTANT:
 * The data pointed to by src must follow the data format described in mist_storage.h
 *
 * @return 0 on success. Otherwise a non-zero value is returned.
 */
int mist_generate_crc(unsigned char *src, unsigned int len)
{
	unsigned char crc;

	if (src == NULL || len < 1)
		return -1;

	verify_lookuptable();

	crc = calculate_crc(src, len - 1);
	src[len - 1] = crc;

	return 0;
}

/**
 * Verifies the CRC of the data pointed to by src.
 *
 * IMPORTANT:
 * The data pointed to by src must follow the data format described in mist_storage.h
 *
 * @return A non-zero value on CRC match, otherwise 0.
 */
int mist_verify_crc(unsigned char *src, unsigned int len)
{
	unsigned char crc;

	if (src == NULL || len < 1)
		return -1;

	verify_lookuptable();

	crc = calculate_crc(src, len - 1);

	return (crc == src[len - 1]);
}

/**
 * Wrapper for reading data from FRAM. Includes retrying the action on error
 * with the number of times specified by MIST_FRAM_ATTEMPTS_READ.
 *
 * The data pointed to by dest DOES NOT NEED to follow the data format
 * described in mist_storage.h.
 */
int mist_fram_read(unsigned char *dest, unsigned int address, unsigned int size)
{
	int ret;
	int attempts;

	if (dest == NULL || size < 1)
		return MIST_STORAGE_ERR_PARAM;

	attempts = 0;
	do {
		ret = FRAM_read(dest, address, size);
		attempts++;
	} while ((ret != 0) && (attempts < MIST_FRAM_ATTEMPTS_READ));

	return ret;
}

/**
 * Wrapper for writing data from FRAM. Includes retrying the action on error
 * with the number of times specified by MIST_FRAM_ATTEMPTS_WRITE.
 *
 * The data pointed to by src DOES NOT NEED to follow the data format
 * described in mist_storage.h.
 */
int mist_fram_write(unsigned char *src, unsigned int address, unsigned int size)
{
	int ret;
	int attempts;

	if (src == NULL || size < 1)
		return MIST_STORAGE_ERR_PARAM;

	attempts = 0;
	do {
		ret = FRAM_writeAndVerify(src, address, size);
		attempts++;
	} while ((ret != 0) && (attempts < MIST_FRAM_ATTEMPTS_WRITE));

	return ret;
}

/**
 * Reads data from FRAM and verifies the CRC.
 *
 * IMPORTANT:
 * The data pointed to by dest must follow the data format described in mist_storage.h
 */
int mist_fram_read_and_verify_crc(unsigned char *dest, unsigned int address, unsigned int size)
{
	int ret;
	verify_lookuptable();

	if (dest == NULL || size < 1)
		return MIST_STORAGE_ERR_PARAM;

	int attempts = 0;
	do {
		ret = FRAM_read(dest, address, size);
		/* No need to convert error code as the mist_storage_errors.h codes
		 * are identical to those in FRAM.h */

		if (ret == 0) {
			unsigned char crc = dest[size - 1]; /* Last byte is CRC */
			if (calculate_crc(dest, size - 1) != crc)
				ret = MIST_STORAGE_ERR_CRC_MISMATCH;
		}

		attempts++;
	} while ((ret != 0) && (attempts < MIST_FRAM_ATTEMPTS_READ));

	return ret;
}


/**
 * Writes to FRAM and verifies the written data. Also calculates the checksum before writing.
 *
 * IMPORTANT:
 * The data pointed to by src must follow the data format described in mist_storage.h
 */
int mist_fram_write_and_generate_crc(unsigned char *src, unsigned int address, unsigned int size)
{
	int ret;
	verify_lookuptable();

	if (src == NULL || size < 1)
		return MIST_STORAGE_ERR_PARAM;

	unsigned char crc = calculate_crc(src, size - 1);
	src[size - 1] = crc;

	int attempts = 0;
	do {
		ret = FRAM_writeAndVerify(src, address, size);
		/* Again, no need to convert the error code. */
		attempts++;
	} while ((ret != 0) && (attempts < MIST_FRAM_ATTEMPTS_WRITE));

	return ret;
}



/**
 * Ensures that the specified directory exists. If a directory exists with that
 * name, then no action is taken. If a file is found with that name, then that
 * file is removed and then a directory is created. If nothing is found with the
 * specified name, then a directory is created.
 *
 * Note: The entered name is fed directly into the HCC API. So try to avoid
 * entering relative paths.
 */
int mist_sdcard_ensure_dir_exists(const char *dirname)
{
	int ret;
	F_FIND find;

	if (dirname == NULL)
		return MIST_STORAGE_ERR_PARAM;

	int attempts = 0;
	do {
		ret = f_findfirst(dirname, &find);
		if (ret == F_NO_ERROR) {
			/* Something was found with this name. Now we only need to do
			 * something if the found entity was a file instead of directory. */
			if (!(find.attr & F_ATTR_DIR)) {
				ret = f_delete(dirname);
				if (ret == F_NO_ERROR) {
					/* Successfully removed file, now create directory */
					ret = f_mkdir(dirname);
				}
			}
		} else if (ret == F_ERR_NOTFOUND) {
			/* No entity with the specified name was found, now we try to
			 * create the directory. If the we are trying to create a
			 * dir called /foo/bar/ when /foo does not exist, then this will
			 * be guaranteed to fail. It is the callers responsibility to
			 * ensure that this does not happen by calling this function with
			 * "/foo" first. */
			ret = f_mkdir(dirname);
		}

		attempts++;
	} while ((ret != F_NO_ERROR) && (attempts < MIST_SDCARD_ATTEMPTS));

	if (ret != F_NO_ERROR)
		ret = MIST_STORAGE_ERR_SDCARD_GENERAL;

	return ret;
}

/**
 * Returns 1 if the entered directory exists, else 0 if the entered directory
 * does not exist or if another error occurred. Unlike
 * mist_sdcard_ensure_dir_exists() this does not create the directory if it was
 * not found;
 */
int mist_sdcard_dir_exists(const char *dirname)
{
	int ret;
	F_FIND find;

	if (dirname == NULL)
		return 0;

	ret = f_findfirst(dirname, &find);
	if (ret != F_NO_ERROR)
		return 0;

	if (!(find.attr & F_ATTR_DIR))
		return 0; /* not a directory */

	return 1; /* entity exists and it is a directory */
}


/**
 * Attempts to remove all files from the specified directory. Sub-directories
 * and files within then are left untouched.
 */
int mist_sdcard_clear_dir(const char *dirname)
{
	int ret, attempts;
	F_FIND find;

	if (dirname == NULL)
		return MIST_STORAGE_ERR_PARAM;

	/* First attempt to enter the specified directory */
	attempts = 0;
	do {
		ret = f_chdir(dirname);
		attempts++;
	} while ((ret != F_NO_ERROR) && (attempts < MIST_SDCARD_ATTEMPTS));

	if (ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_GENERAL;

	/* Now try to empty the directory */
	ret = f_findfirst("*.*", &find);
	while (ret == F_NO_ERROR) {
		/* Delete file if it is not a directory or a hidden file. */
		if (find.filename[0] != '.' && !(find.attr & F_ATTR_DIR))
			f_delete(find.filename);

		ret = f_findnext(&find);
	}

	/* TODO: Check error code here */

	return MIST_STORAGE_OK;
}

/**
 * Formats a filename according to the entered id. The resulting filename will
 * be in a simplified hexadecimal representation. Instead of using symbols
 * 0123456789ABCDEF we use ABCDEFGHIJKLMNOP for efficiency. So if the entered
 * id is 0x00000014 then the filename becomes "AAAAAABE".
 */
int mist_sdcard_generate_filename(char fname[9], uint32_t id)
{
	if (fname == NULL)
		return MIST_STORAGE_ERR_PARAM;

	fname[0] = 'A' + ((id >> 28) & 0xF);
	fname[1] = 'A' + ((id >> 24) & 0xF);
	fname[2] = 'A' + ((id >> 20) & 0xF);
	fname[3] = 'A' + ((id >> 16) & 0xF);
	fname[4] = 'A' + ((id >> 12) & 0xF);
	fname[5] = 'A' + ((id >> 8) & 0xF);
	fname[6] = 'A' + ((id >> 4) & 0xF);
	fname[7] = 'A' + (id & 0xF);
	fname[8] = 0;

	return 0;
}

/**
 * Reads a file with the specified file_id from the entered directory dirname.
 * The filename of the file will be /<dirname>/<"%08X" % file_id>. E.g. if
 * dirname is "/mist_tm/0" and file id is 260 then the filename will be
 * /mist_tm/0/AAAAAABE.
 *
 * Specified dirname must be an absolute path from the root.
 * destsize must be larger than 0.
 */
int mist_sdcard_read_id(const char *dirname, uint32_t file_id, uint8_t *dest, uint32_t destsize, uint32_t *bytes_read)
{
	int ret;
	long filelen, readlen;
	char fname[9];
	F_FILE *file;

	if (dirname == NULL || dest == NULL || destsize == 0 || bytes_read == NULL)
		return MIST_STORAGE_ERR_PARAM;

	ret = f_chdir(dirname); /* Since we are using cooperative scheduling, no
	                         * other function should manage to change the
	                         * directory before we read from the file. */
	if (ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_DIR;

	ret = mist_sdcard_generate_filename(fname, file_id);
	if (ret != 0)
		return ret;

	filelen = f_filelength(fname);
	if (filelen < 0)
		return MIST_STORAGE_ERR_SDCARD_METADATA;
	if (destsize < (uint32_t) filelen)
		return MIST_STORAGE_ERR_BUFFER_TOO_SMALL;

	file = f_open(fname, "r");
	if (!file)
		return MIST_STORAGE_ERR_SDCARD_READ;

	readlen = f_read_mtu(dest, filelen, file);
	ret = f_close(file);
	if (readlen != filelen || ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_READ;

	*bytes_read = (uint32_t) readlen;

	return MIST_STORAGE_OK;
}

/**
 * Reads a fixed number of bytes from a file with the specified file_id from
 * the entered directory dirname. The offset specifies where in the file to
 * start reading from.
 */
int mist_sdcard_fixedread_id(const char *dirname, uint32_t file_id, uint8_t *dest, uint32_t offset, uint32_t len)
{
	int ret;
	long seekret = 0;
	long loff, llen;
	long readlen = 0;
	char fname[9];
	F_FILE *file;

	loff = (long) offset;
	llen = (long) len;

	if (dirname == NULL || dest == NULL)
		return MIST_STORAGE_ERR_PARAM;
	if (loff < 0 || llen <= 0)
		return MIST_STORAGE_ERR_PARAM; // overflow error (or len is 0)

	ret = f_chdir(dirname);
	if (ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_DIR;

	ret = mist_sdcard_generate_filename(fname, file_id);
	if (ret != 0)
		return ret;

	file = f_open(fname, "r");
	if (!file)
		return MIST_STORAGE_ERR_SDCARD_READ;

	seekret = f_seek(file, loff, F_SEEK_SET);
	if (seekret == F_NO_ERROR) {
		readlen = f_read_mtu(dest, llen, file);
	}

	ret = f_close(file);
	if (seekret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_SEEK;
	if (readlen != llen || ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_READ;

	return MIST_STORAGE_OK;
}

/**
 * Writes to a file with the specified file_id in the entered directory name.
 * The name of the file is determined by the mist_sdcard_generate_filename
 * function. See mist_sdcard_read_id for example filename.
 *
 * Specified dirname must be an absolute path from the root.
 * The length of the data must be greater than 0.
 *
 * Uses SafeFAT.
 */
int mist_sdcard_write_id(const char *dirname, uint32_t file_id, const uint8_t *src, uint32_t len)
{
	int ret;
	long writelen;
	char fname[9];
	F_FILE *file;

	if (dirname == NULL || src == NULL || len == 0)
		return MIST_STORAGE_ERR_PARAM;

	ret = f_chdir(dirname);
	if (ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_DIR;

	ret = mist_sdcard_generate_filename(fname, file_id);
	if (ret != 0)
		return ret;

	file = f_open(fname, "w+");
	if (!file)
		return MIST_STORAGE_ERR_SDCARD_WRITE;

	writelen = f_write_mtu(src, len, file);
	ret = f_close(file);
	if (writelen < 0 || (uint32_t) writelen != len || ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_WRITE;

	return MIST_STORAGE_OK;
}

/**
 * Writes to a file with the specified file_id in the entered directory name
 * with a specified offset. The name of the file is determined by the
 * mist_sdcard_generate_filename function. See mist_sdcard_read_id for
 * example filename.
 *
 * If the offset is larger than the current file, the offset will be padded
 * with zeros.
 *
 * Specified dirname must be an absolute path from the root.
 * The length of the data must be greater than 0.
 *
 * Uses SafeFAT.
 */
int mist_sdcard_fixedwrite_id(const char *dirname, uint32_t file_id, const uint8_t *src, uint32_t offset, uint32_t len)
{
	int ret;
	long seekret = 0;

	long writelen = 0;
	long loffset;
	char fname[9];
	F_FILE *file;

	loffset = (long) offset;

	if (dirname == NULL || src == NULL || len == 0)
		return MIST_STORAGE_ERR_PARAM;
	if (loffset < 0)
		return MIST_STORAGE_ERR_PARAM;

	ret = f_chdir(dirname);
	if (ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_DIR;

	ret = mist_sdcard_generate_filename(fname, file_id);
	if (ret != 0)
		return ret;

	file = f_open(fname, "r+");
	if (!file)
		return MIST_STORAGE_ERR_SDCARD_WRITE;

	seekret = f_seek(file, loffset, F_SEEK_SET);
	if (seekret == F_NO_ERROR) {
		writelen = f_write_mtu(src, len, file);
	}

	ret = f_close(file);
	if (seekret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_SEEK;
	if (writelen < 0 || (uint32_t) writelen != len || ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_WRITE;

	return MIST_STORAGE_OK;
}

/**
 * Deletes the file with the specified id from the specified directory.
 */
int mist_sdcard_delete_id(const char *dirname, uint32_t file_id)
{
	int ret;
	char fname[9];

	if (dirname == NULL)
		return MIST_STORAGE_ERR_PARAM;

	ret = f_chdir(dirname);
	if (ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_DIR;

	ret = mist_sdcard_generate_filename(fname, file_id);
	if (ret != 0)
		return ret;

	ret = f_delete(fname);
	if (ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_DELETE;

	return MIST_STORAGE_OK;
}

/**
 * Checks if a file with the specified id exists in the directory. Returns 0 if
 * the file does not exist or if an error occurred. Returns 1 if no error
 * occurred and the file was found.
 */
int mist_sdcard_exists_id(const char *dirname, uint32_t file_id)
{
	char fname[9];

	int ret;
	F_STAT stat;

	ret = mist_sdcard_generate_filename(fname, file_id);
	if (ret != 0)
		return 0;

	if (dirname == NULL)
		return 0;

	ret = f_chdir(dirname);
	if (ret != F_NO_ERROR)
		return 0;

	ret = f_stat(fname, &stat);
	if (ret != F_NO_ERROR)
		return 0;

	return 1; /* All tests passed, file seems to exist. */
}


/**
 * Creates an empty file to the specified size, filled with 0
 */
int mist_sdcard_create_zeroed_file_id(const char *dirname, uint32_t file_id, uint32_t size)
{
	char fname[9];
	uint8_t zero = 0;

	int ret;
	long l_size = (long) size;
	long seekret = 0;
	long writelen = 0;

	F_FILE *file;

	if (dirname == NULL)
		return MIST_STORAGE_ERR_PARAM;

	if (mist_sdcard_exists_id(dirname, file_id) || l_size < 0)
		return MIST_STORAGE_ERR_PARAM; //File already exists or size overflow

	ret = mist_sdcard_generate_filename(fname, file_id);
	if (ret != 0)
		return ret;

	ret = f_chdir(dirname);
	if (ret != F_NO_ERROR)
		return MIST_STORAGE_ERR_PARAM;

	file = f_open(fname, "w");
	if (!file)
		return MIST_STORAGE_ERR_SDCARD_WRITE;

	if (l_size > 0) {
		seekret = f_seek(file, l_size - 1, F_SEEK_SET);
		if (seekret == F_NO_ERROR) {
			writelen = f_write_mtu(&zero, 1, file);
		}
	}

	ret = f_close(file);
	if (seekret != F_NO_ERROR)
		return MIST_STORAGE_ERR_SDCARD_SEEK;
	if (ret != F_NO_ERROR || writelen != 1)
		return MIST_STORAGE_ERR_SDCARD_WRITE;

	return MIST_STORAGE_OK;
}

/**
 * Returns the length of a specified file in number of bytes.
 * Returns 0 if the file does not exist.
 */
uint32_t mist_sdcard_length_id(const char *dirname, uint8_t file_id)
{
	int ret;
	char fname[9];
	long length;

	if (dirname == NULL)
		return 0;

	if (f_chdir(dirname) != F_NO_ERROR)
		return 0;

	ret = mist_sdcard_generate_filename(fname, file_id);
	if (ret != 0)
		return 0;

	length = f_filelength(fname);
	if (length < 0)
		return 0;

	return (uint32_t) length;
}
